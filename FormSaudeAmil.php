<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-contact.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body><br><br>
    <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-bordered p-form-lg">
                  <?php if( $form->isValid ) { ?>
                      <script type="text/javascript">
                          setTimeout(function () {
                              window.parent.location.href = "http://landingpages.planosdesaude360.com.br/amil/obrigado.php"; //will redirect to your blog page (an ex: blog.html)
                          }, 1);
                      </script>                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $form->attributeView( 'possuicnpj' ); ?>
                            </div>
                            <div class="col-sm-8">
                                <?php $form->attributeView( 'cnpj' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'text' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'email' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'telefone' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'telefone_alternativo' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'cidade' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'qtd_pessoas' ); ?>
                            </div>
                            <div class="col-sm-6" style="display:none">
                                <?php $form->attributeView( 'idfonte' ); ?>
                            </div>
                            <div class="col-sm-12">
                                <?php $form->attributeView( 'textarea' ); ?>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="text-right">
                            <button class="btn" type="submit" name="confirm"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;send message</button>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>