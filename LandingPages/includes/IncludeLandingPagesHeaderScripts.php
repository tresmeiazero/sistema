        <meta name="robots" content="noindex, nofollow" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />

        <meta name="description" content="" />
        <meta name="keywords" content="Plano de Saúde, Plano de Saúde Intermedica, Intermedica Saúde" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Agência TresMeiaZero" />
        <meta property="og:description" content="Os Preços dos Planos de Saúde Intermedica variam de acordo com a idade dos beneficiários e do tipo de Plano contratado.Os Planos cotados com CNPJ (PME e Empresarial) são até 30% mais baratos do que os Planos por adesão (vinculados à sua entidade de classe: OAB, CREA, CREF, entre outros)&#38;nbsp;" />
        <meta property="og:site_name" content="Agência TresMeiaZero" />
        <meta property="og:url" content="http://lp.360planodesaude.com.br/saude/amil/" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <link href="https://fonts.googleapis.com/css?family=Khand:300,400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
        <link href="LandingPages/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="LandingPages/assets/css/LandingPagesSaudeIntermedica.css" rel="stylesheet" />
