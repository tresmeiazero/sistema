<?php

namespace FormsPlus\Framework\Helper;

use FormsPlus\Framework\FormsPlusFramework;
use FormsPlus\Framework\Helper\AdminPanel;
use FormsPlus\Framework\Helper\Debug;
use FormsPlus\Framework\Helper\Configuration;
use FormsPlus\Framework\Helper\Storage;
use FormsPlus\Framework\Helper\UnitFormat;

class AdminPanel
{
    private $configFile;
    private $configuration;
    private $storage;
    private $session;
    private $isLoggedInUser = false;

    private $variableList = array(
        'asset_directory_uri'    => './',
        'translation_dictionary' => '',
        'template_directory'     => '',
        'locale'                 => 'pt_BR',
        'layout'                 => 'base.html.php',
        'content_info'           => array(),
        'base_url'               => '',
        'style_list'             => array(
            100 => 'font-awesome/css/font-awesome.min.css',
            200 => 'bootstrap/css/bootstrap.min.css',
            300 => 'gantella/css/custom.css',
            400 => 'css/admin.css'
        ),
        'script_list'            => array(
            100 => 'js/jquery.js',
            200 => 'bootstrap/js/bootstrap.min.js',
            300 => 'gantella/js/custom.js',
            400 => 'js/admin.js'
        )
    );

    public function __construct( $configFile, $optionList = array() )
    {
        $this->configFile = $configFile;
        $configuration = new Configuration( $this );
        $this->configuration = $configuration->getConfig();
        if( isset( $optionList[ 'asset_directory_uri' ] ) ) {
            $this->variableList[ 'asset_directory_uri' ] = strval( $optionList[ 'asset_directory_uri' ] );
        }
        $applicationDirectory = FormsPlusFramework::getApplicationDirectory();
        $this->variableList[ 'template_directory' ] = $applicationDirectory . '/src/FormsPlus/Framework/Resources/views/adminPanel/';
        $this->variableList[ 'base_url' ] = $this->getBaseURL();
        $this->sendDefaultHeaders();
        // if users are not defind in the config file then the admin panel cannot be used
        if( empty( $this->configuration[ 'administration' ][ 'access' ][ 'user_list' ] ) ) {
            Debug::errorMessage( 'Users must be defined in the config file in order to access this page', $configFile );
            echo Debug::getMessageListHTML();
            $this->terminateScript();
        }
        // run a user authorization, so only logged users are allowed to use the admin panel
        $this->authorization();
        $this->storage = Storage::newInstance( $this );
        // if the storage is not ready then the admin panel cannot be used
        if( !$this->storage ) {
            echo Debug::getMessageListHTML();
            $this->terminateScript();
        }
        // if the storage is "none" then the admin panel is useless
        if( $this->storage instanceof FormsPlus\Framework\Helper\Storage\None ) {
            echo( 'The storage is turned off by the config file. There is no need for admin panel.' );
            echo Debug::getMessageListHTML();
            $this->terminateScript();
        }
        // get a default dictionary of a default locale to translate messages
        $this->variableList[ 'translation_dictionary' ] = include $applicationDirectory . '/src/FormsPlus/Framework/Resources/translations/messages.en.php';
        $userLocale = $this->session->getSegment( 'FormsPlus\AdminPanel' )->get( 'locale' );
        if( $this->isLoggedInUser && $userLocale ) {
            $this->variableList[ 'locale' ] = $userLocale;
        } elseif( isset( $this->configuration[ 'locale' ][ 'admin_panel' ][ 0 ] ) ) {
            $this->variableList[ 'locale' ] = $this->configuration[ 'locale' ][ 'admin_panel' ][ 0 ];
        }
        if( file_exists( ( $dictionaryFile = $applicationDirectory . '/app/translations/messages.' . $this->variableList[ 'locale' ] . '.php' ) ) ) {
            $this->variableList[ 'translation_dictionary' ] = array_replace( $this->variableList[ 'translation_dictionary' ], include $dictionaryFile );
        } elseif( file_exists( ( $dictionaryFile = $applicationDirectory . '/src/FormsPlus/Framework/Resources/translations/messages.' . $this->variableList[ 'locale' ] . '.php' ) ) ) {
            $this->variableList[ 'translation_dictionary' ] = array_replace( $this->variableList[ 'translation_dictionary' ], include $dictionaryFile );
        }
        // initialize action and process templates
        $result = $this->initializeAction();
        if( isset( $result[ 'content' ] ) || isset( $result[ 'error' ] ) ) {
            $this->includeTemplate( $this->variableList[ 'layout' ], $result );
        }
    }

    public function __get( $name )
    {
        return isset( $this->$name ) ? $this->$name : null;
    }

    private function sendDefaultHeaders()
    {
        header( 'Cache-Control: no-store, no-cache, must-revalidate, max-age=0' );
        header( 'Cache-Control: post-check=0, pre-check=0', false );
        header( 'Pragma: no-cache' );
    }

    private function authorization()
    {
        $sessionFactory = new \Aura\Session\SessionFactory;
        $this->session = $sessionFactory->newInstance( $_COOKIE );
        $this->session->setName( 'FPSESSID' );
        $sessionSegment = $this->session->getSegment( 'FormsPlus\AdminPanel' );
        if( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'login' ) {
            return;
        }
        if( $sessionSegment->get( 'username' ) && isset( $this->configuration[ 'administration' ][ 'access' ][ 'user_list' ][ $sessionSegment->get( 'username' ) ] ) ) {
            $this->isLoggedInUser = true;
        } else {
            $this->session->destroy();
        }
        if( !$this->isLoggedInUser ) {
            $this->pageRedirect( array( 'action' => 'login' ) );
        }
        if( isset( $_GET[ 'action' ] ) && $_GET[ 'action' ] == 'logout' ) {
            $this->session->destroy();
            $this->pageRedirect( array( 'action' => 'login' ) );
        }
    }

    private function pageRedirect( $params = array() )
    {
        $url = $_SERVER[ 'PHP_SELF' ];
        $url .= !empty( $params ) ? '?' . http_build_query( $params ) : '';
        header( 'Location: ' . $url );
        $this->terminateScript();
    }

    private function getBaseURL()
    {
        $url = ( isset( $_SERVER[ 'HTTPS' ] ) && $_SERVER[ 'HTTPS' ] == 'on' ) ? 'https://' : 'http://';
        $url .= $_SERVER[ 'SERVER_NAME' ];
        if( $_SERVER[ 'SERVER_PORT' ] != '80' && $_SERVER[ 'SERVER_PORT' ] != '443') {
            $url .= ':' . $_SERVER[ 'SERVER_PORT' ];
        }
        return $url;
    }

    private function pageLink( $params = array(), $full = false )
    {
        $url = $full ? $this->variableList[ 'base_url' ] : '';
        $url .= $_SERVER[ 'PHP_SELF' ];
        $url .= !empty( $params ) ? '?' . http_build_query( $params ) : '';
        return $url;
    }

    private function generateFileAttributeHtml( $collectionID, $attributeID, $attributeFileList, $index = null, $fullURL = false, $pattern = '<a target="_blank" href="%link%">%name%</a> (%size%)' )
    {
        $attributeValue = '';
        reset( $attributeFileList );
        $attributeFileIndexFirst = key( $attributeFileList );
        if( isset( $attributeFileList[ $attributeFileIndexFirst ][ 'name' ] ) ) {
            foreach( $attributeFileList as $attributeFileIndex => $attributeFile ) {
                $attributeValue .= ( $attributeFileIndex != $attributeFileIndexFirst ) ? ', ' : '';
                $attributeValue .= str_replace(
                    array( '%link%', '%name%', '%size%' ),
                    array( $this->pageLink( array( 'action' => 'fileDownload', 'collectionID' =>  $collectionID, 'attributeID' => $attributeID, 'fileIndex' => ( $index !== null ) ? array( $index, $attributeFileIndex ) : array( $attributeFileIndex ) ), $fullURL ), $attributeFile[ 'name' ],  UnitFormat::humanFilesize( $attributeFile[ 'size' ] ) ),
                    $pattern
                );
            }
        } else {
            foreach( $attributeFileList as $attributeFileIndex => $attributeFile ) {
                $attributeValue .= ( $attributeFileIndex != $attributeFileIndexFirst ) ? '; ' : '';
                reset( $attributeFile );
                $fileIndexFirst = key( $attributeFile );
                foreach( $attributeFile as $fileIndex => $file ) {
                    $attributeValue .= ( $fileIndex != $fileIndexFirst ) ? ', ' : '';
                    $attributeValue .= str_replace(
                        array( '%link%', '%name%', '%size%' ),
                        array( $this->pageLink( array( 'action' => 'fileDownload', 'collectionID' => $collectionID, 'attributeID' => $attributeID, 'fileIndex' => array( $attributeFileIndex, $fileIndex ) ), $fullURL ), $file[ 'name' ],  UnitFormat::humanFilesize( $file[ 'size' ] ) ),
                        $pattern
                    );
                }
            }
        }
        return $attributeValue;
    }

    private function terminateScript()
    {
        exit;
    }

    public function includeTemplate( $templatePath, $templateVars = array() )
    {
        $templateFullPath = $this->variableList[ 'template_directory' ] . $templatePath;
        foreach( $templateVars as $varName => $varValue ) {
            $$varName = $varValue;
        }
        include $templateFullPath;
    }

    public function translateText( $textID, $replacementList = false )
    {
        $text = $textID;
        if( isset( $this->variableList[ 'translation_dictionary' ][ $textID ] ) ) {
            $text = $this->variableList[ 'translation_dictionary' ][ $textID ];
        }
        if( $replacementList !== false ) {
            $text = str_replace( array_keys( $replacementList ), array_values( $replacementList ), $text );
        }
        return $text;
    }

    private function initializeAction()
    {
        $action = 'formList';
        if( !empty( $_GET[ 'action' ] ) && file_exists( $this->variableList[ 'template_directory' ] . 'action/' . $_GET[ 'action' ] . '.html.php' ) ) {
            $action = $_GET[ 'action' ];
        }
        $this->variableList[ 'content_info' ][ 'action' ] = $action;
        ob_start();
        $result = include $this->variableList[ 'template_directory' ] . 'action/' . $action . '.html.php';
        $content = ob_get_contents();
        ob_end_clean();
        ksort( $this->variableList[ 'style_list' ] );
        ksort( $this->variableList[ 'script_list' ] );
        if( is_array( $result ) ) {
            return $result;
        }
        return array( 'content' => $content );
    }
}
