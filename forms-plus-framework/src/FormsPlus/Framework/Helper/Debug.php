<?php

namespace FormsPlus\Framework\Helper;

class Debug
{
    const NOTICE = 1;
    const WARNING = 2;
    const ERROR = 3;

    private static $messageList = array();

    public static function noticeMessage( $content, $target = '' )
    {
        self::$messageList[] = array( time(), self::NOTICE, $content, $target );
    }

    public static function warningMessage( $content, $target = '' )
    {
        self::$messageList[] = array( time(), self::WARNING, $content, $target );
    }

    public static function errorMessage( $content, $target = '' )
    {
        self::$messageList[] = array( time(), self::ERROR, $content, $target );
    }

    public static function getMessageListHTML( $wrapperHTML = '<div style="background: white;">%content%</div>' )
    {
        $messageTypeList = array(
            self::NOTICE  => array( 'Notice', 'green' ),
            self::WARNING => array( 'Warning', 'orange' ),
            self::ERROR   => array( 'Error', 'red' )
        );
        $result = '';
        foreach( self::$messageList as $message ) {
            $result .= '<div><span style="color: gray;">' . date( '[H:i:s]', $message[ 0 ] ) . '</span>';
            if( !empty( $message[ 3 ] ) ) {
                $result .= ' <b style="color: gray;">' . $message[ 3 ] . '.</b>';
            }
            $result .= ' <b style="color: ' . $messageTypeList[ $message[ 1 ] ][ 1 ] . ';">' . $messageTypeList[ $message[ 1 ] ][ 0 ] . '.</b> ';
            $result .= $message[ 2 ] . '</div>';
        }
        if( !empty( $result ) ) {
            $result = str_replace( '%content%', $result, $wrapperHTML );
        }
        return $result; 
    }

    public static function clear()
    {
        self::$messageList = array();
    }
}