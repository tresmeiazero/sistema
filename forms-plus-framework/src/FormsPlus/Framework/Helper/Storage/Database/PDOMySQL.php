<?php

namespace FormsPlus\Framework\Helper\Storage\Database;

use FormsPlus\Framework\Helper\Storage\Database;
use FormsPlus\Framework\Helper\Debug;

class PDOMySQL extends Database
{
    private $db;

    public function __construct( $framework )
    {
        $this->framework = $framework;
        $this->getConnectionParameters();
        $this->connect();
    }

    private function connect()
    {
        try {
            $this->db = new \PDO(
                'mysql:host=' . $this->connectionParameters[ 'host' ] . ';' . ( !empty( $this->connectionParameters[ 'port' ] ) ? 'port=' . $this->connectionParameters[ 'port' ] . ';' : '' ) . 'dbname=' . $this->connectionParameters[ 'name' ] . ';charset=utf8',
                $this->connectionParameters[ 'user' ],
                $this->connectionParameters[ 'password' ]
            );
        } catch( \PDOException $e ) {
            Debug::errorMessage( 'PDO MySQL connection error: ' . $e->getMessage(), $this->framework->configFile );
            return;
        }
        $this->isConnected = true;
    }

    public function escapeString( $value )
    {
        return $this->db->quote( $value );
    }

    public function queryInsertID()
    {
        return $this->db->lastInsertId();
    }

    public function resultQuery( $query, $values = array() )
    {
        try {
            $stmt = $this->db->prepare( $this->injectValuesIntoQuery( $query, $values ) );
            $stmt->execute();
            $result = $stmt->fetchAll( \PDO::FETCH_ASSOC );
        } catch( \PDOException $e ) {
            Debug::errorMessage( 'PDO MySQL result query error: ' . $e->getMessage(), $this->framework->configFile );
            return false;
        }
        return $result;
    }

    public function query( $query, $values = array() )
    {
        try {
            $stmt = $this->db->prepare( $this->injectValuesIntoQuery( $query, $values ) );
            $stmt->execute();
        } catch( \PDOException $e ) {
            Debug::errorMessage( 'PDO MySQL query error: ' . $e->getMessage(), $this->framework->configFile );
            return false;
        }
        return true;
    }
}
