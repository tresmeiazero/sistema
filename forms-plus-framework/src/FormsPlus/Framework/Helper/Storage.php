<?php

namespace FormsPlus\Framework\Helper;

use FormsPlus\Framework\FormsPlusFramework;

class Storage
{
    protected $framework;
    protected $filesDirectory;
    private static $preparedFilesDirectoryList = array();

    public function __get( $name )
    {
        return isset( $this->$name ) ? $this->$name : null;
    }

    public static function newInstance( $framework )
    {
        $handlerList = array(
            'database' => 'Database',
            'file'     => 'File',
            'none'     => 'None'
        );
        $storageType = 'none';
        if( !empty( $framework->configuration[ 'storage' ][ 'type' ] ) ) {
            $storageType = $framework->configuration[ 'storage' ][ 'type' ];
        }
        if( !isset( $handlerList[ $storageType ] ) ) {
            Debug::errorMessage( 'Unknown storage type "' . $storageType . '", it supports: database, none.', $framework->configFile );
            return;
        }
        $className = 'FormsPlus\Framework\Helper\Storage\\' . $handlerList[ $storageType ];
        $instance = $className::newInstance( $framework );
        if( !$instance ) {
            return;
        }
        $instance->filesDirectory = FormsPlusFramework::getApplicationDirectory() . '/var/storage/files';
        if( !$instance->prepareStorage() ) {
            Debug::errorMessage( 'The storage is not ready to store the collected data from the form.', $framework->configFile );
            return;
        }
        return $instance;
    }

    protected function prepareFilesDirectory()
    {
        // it prevents multiple preparations for one files directory
        if( in_array( $this->filesDirectory, self::$preparedFilesDirectoryList ) ) {
            return true;
        }
        if( !( file_exists( $this->filesDirectory ) && is_dir( $this->filesDirectory ) ) ) {
            Debug::warningMessage( 'The files directory is missing (forms-plus-framework/var/storage/files).', $this->framework->configFile );
            if( !mkdir( $this->filesDirectory, 0755, true ) ) {
                Debug::errorMessage( 'The "forms-plus-framework/var/storage/files" directory cannot be created (lack of permission).', $this->framework->configFile );
                return false;
            }
        }
        if( !is_writable( $this->filesDirectory ) ) {
            Debug::errorMessage( 'The "' . $this->filesDirectory . '" is not writable, the files cannot be stored.', $this->framework->configFile );
            return false;
        }
        self::$preparedFilesDirectoryList[] = $this->filesDirectory;
        return true;
    }

    protected function createDirectory( $fullPath )
    {
        if( !mkdir( $fullPath, 0755, true ) ) {
            Debug::errorMessage( 'The "' . $fullPath . '" cannot be created "' . $this->filesDirectory . '".', $this->framework->configFile );
            return false;
        }
        return true;
    }

    protected function getDataToStore()
    {
        $data = array();
        if( !empty( $this->framework->data[ 'file_list' ] ) ) {
            $fileListData = $this->framework->data[ 'file_list' ];
            foreach( $fileListData as $attributeID => &$attributeFileList ) {
                foreach( $attributeFileList as &$attributeFile ) {
                    if( isset( $attributeFile[ 'name' ] ) ) {
                        $attributeFile[ 'per_name' ] = basename( $attributeFile[ 'tmp_name' ] );
                        unset( $attributeFile[ 'tmp_name' ] );
                        unset( $attributeFile[ 'error' ] );
                    } else {
                        foreach( $attributeFile as &$file ) {
                            $file[ 'per_name' ] = basename( $file[ 'tmp_name' ] );
                            unset( $file[ 'tmp_name' ] );
                            unset( $file[ 'error' ] );
                        }
                    }
                }
            }
        }
        if( isset( $fileListData ) ) {
            $data[ 'file_list' ] = $fileListData;
        }
        foreach( $this->framework->data[ 'attribute_list' ] as $attributeID => $attribute ) {
            if( is_array( $attribute[ 'string_value' ] ) ) {
                foreach( $attribute[ 'string_value' ] as $index => $stringValue ) {
                    if( $stringValue != '' ) {
                        $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ][ $index ] = $stringValue;
                    }
                }
            }
        }
        return $data;
    }

    protected function storeFiles( $fileDirPath )
    {
        // it's pointless to keep the data of the files if the files itself cannot be stored.
        if( empty( $this->framework->data[ 'file_list' ] ) ) {
            return;
        }
        if( !is_writable( $this->filesDirectory ) ) {
            Debug::errorMessage( 'The "' . $this->filesDirectory . '" is not writable, the directory for the collected files cannot be created there.', $this->framework->configFile );
            return;
        }
        if( !$this->createDirectory( $this->filesDirectory . '/' . $fileDirPath ) ) {
            return;
        }
        foreach( $this->framework->data[ 'file_list' ] as $attributeFileList ) {
            foreach( $attributeFileList as $attributeFile ) {
                $fileList = isset( $attributeFile[ 'name' ] ) ? array( $attributeFile ) : $attributeFile;
                foreach( $fileList as $file ) {
                    $tmpFile = $file[ 'tmp_name' ];
                    $perFile = $this->filesDirectory . '/' . $fileDirPath . '/' . basename( $tmpFile );
                    if( !copy( $tmpFile, $perFile ) ) {
                        Debug::errorMessage( 'Filed to copy file from "' . $tmpFile . '" to "' . $perFile . '".', $this->framework->configFile );
                    }
                }
            }
        }
    }

    protected function removeDirectory( $path, $onlyEmpty = false )
    {
        if( !file_exists( $path ) ) {
            return;
        }
        if( $onlyEmpty && is_dir( $path ) ) {
            if( count( glob( $path . '/*' ) ) === 0 ) {
                if( !rmdir( $path ) ) {
                    Debug::errorMessage( 'The "' . $path . '" directory cannot be removed.', $this->framework->configFile );
                }
            }
        } else {
            if( !$this->removeDirectoryRecursively( $path ) ) {
                Debug::errorMessage( 'The "' . $path . '" directory cannot be removed.', $this->framework->configFile );
            }
        }
    }

    private function removeDirectoryRecursively( $path ) {
        $fileList = glob( $path . '/*' );
        foreach ( $fileList as $file ) {
            is_dir( $file ) ? $this->removeDirectoryRecursively( $file ) : unlink( $file );
        }
        return rmdir( $path );
    }
}
