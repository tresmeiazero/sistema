<?php

namespace FormsPlus\Framework\Helper;

class Template
{
    public static function generateHTMLAttributes( $attributes )
    {
        $html = '';
        foreach( $attributes as $attribute => $value ) {
            $html .= ' ' . $attribute . ( ( is_bool( $value ) && $value ) ? '' : '="' . htmlspecialchars( $value, ENT_QUOTES, 'UTF-8' ) . '"' );
        }
        return $html;
    }

    public static function setInputBaseAttributes( &$inputAttributeList, &$attribute )
    {
        if( !empty( $attribute[ 'placeholder' ] ) ) {
            $inputAttributeList[ 'placeholder' ] = $attribute[ 'placeholder' ];
        }
        if( !empty( $attribute[ 'autocomplete' ] ) ) {
            $inputAttributeList[ 'autocomplete' ] = $attribute[ 'autocomplete' ];
        }
        if( isset( $attribute[ 'required' ] ) && $attribute[ 'required' ] ) {
            $inputAttributeList[ 'required' ] = true;
        }
        if( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) {
            $inputAttributeList[ 'disabled' ] = true;
        }
        if( isset( $attribute[ 'readonly' ] ) && $attribute[ 'readonly' ] ) {
            $inputAttributeList[ 'readonly' ] = true;
        }
        if( isset( $attribute[ 'autofocus' ] ) && $attribute[ 'autofocus' ] ) {
            $inputAttributeList[ 'autofocus' ] = true;
        }
    }

    public static function setInputStringAttributes( &$inputAttributeList, &$attribute )
    {
        if( isset( $attribute[ 'minlength' ] ) && $attribute[ 'minlength' ] > 0 ) {
            $inputAttributeList[ 'minlength' ] = $attribute[ 'minlength' ];
        }
        if( isset( $attribute[ 'maxlength' ] ) && $attribute[ 'maxlength' ] > 0 ) {
            $inputAttributeList[ 'maxlength' ] = $attribute[ 'maxlength' ];
        }
    }

    public static function setInputNumberAttributes( &$inputAttributeList, &$attribute )
    {
        if( isset( $attribute[ 'min' ] ) ) {
            $inputAttributeList[ 'min' ] = $attribute[ 'min' ];
        }
        if( isset( $attribute[ 'max' ] ) ) {
            $inputAttributeList[ 'max' ] = $attribute[ 'max' ];
        }
    }

    public static function setInputValidationAttributes( &$inputAttributeList, &$attribute )
    {
        if( !empty( $attribute[ 'validation' ][ 'rule_list' ] ) ) {
            foreach( $attribute[ 'validation' ][ 'rule_list' ] as $ruleName => $ruleValue ) {
                if( substr( $ruleName, 0, 7 ) === 'backend' ) {
                    continue;
                }
                if( is_array( $ruleValue ) ) {
                    $inputAttributeList[ 'data-rule-' . str_replace( '_', '-', $ruleName ) ] = json_encode( $ruleValue );
                } elseif( is_bool( $ruleValue ) ) {
                     $inputAttributeList[ 'data-rule-' . str_replace( '_', '-', $ruleName ) ] = $ruleValue ? 'true' : 'false';
                } else {
                    $inputAttributeList[ 'data-rule-' . str_replace( '_', '-', $ruleName ) ] = $ruleValue;
                }
            }
        }
        if( !empty( $attribute[ 'validation' ][ 'message_list' ] ) ) {
            foreach( $attribute[ 'validation' ][ 'message_list' ] as $ruleName => $messageText ) {
                if( substr( $ruleName, 0, 7 ) === 'backend' ) {
                    continue;
                }
                $inputAttributeList[ 'data-msg-' . str_replace( '_', '-', $ruleName ) ] = $messageText;
            }
        }
        if( !empty( $attribute[ 'validation' ][ 'valid_message' ] ) ) {
            $inputAttributeList[ 'data-valid-msg' ] = $attribute[ 'validation' ][ 'valid_message' ];
        }
        if( !empty( $attribute[ 'validation' ][ 'valid_icon' ] ) ) {
            $inputAttributeList[ 'data-valid-icon' ] = $attribute[ 'validation' ][ 'valid_icon' ];
        }
        if( !empty( $attribute[ 'validation' ][ 'error_icon' ] ) ) {
            $inputAttributeList[ 'data-error-icon' ] = $attribute[ 'validation' ][ 'error_icon' ];
        }
        if( isset( $attribute[ 'validation' ][ 'highlight_state_message' ] ) && !is_null( $attribute[ 'validation' ][ 'highlight_state_message' ] ) ) {
            $inputAttributeList[ 'data-js-highlight-state-msg' ] = $attribute[ 'validation' ][ 'highlight_state_message' ] ? 'true' : 'false';;
        }
        if( isset( $attribute[ 'validation' ][ 'show_valid_message' ] ) && !is_null( $attribute[ 'validation' ][ 'show_valid_message' ] ) ) {
            $inputAttributeList[ 'data-show-valid-msg' ] = $attribute[ 'validation' ][ 'show_valid_message' ] ? 'true' : 'false';;
        }
        $class = '';
        if( isset( $attribute[ 'validation' ][ 'ignore' ] ) && $attribute[ 'validation' ][ 'ignore' ] ) {
            $class .= ' p-ignore-field';
        } elseif( isset( $attribute[ 'validation' ][ 'ignore_hidden' ] ) && $attribute[ 'validation' ][ 'ignore_hidden' ] ) {
            $class .= ' p-ignore-hidden-field';
        }
        if( !empty( $class ) ) {
            $inputAttributeList[ 'class' ] = isset( $inputAttributeList[ 'class' ] ) ? $inputAttributeList[ 'class' ] . $class : $class;
        }
    }

    public static function setPluginAttributes( &$inputAttributeList, &$attribute, $attributeID, $pluginAttributeName = '' )
    {
        if( !empty( $attribute[ $attributeID ] ) ) {
            $inputAttributeNamePrefix = 'data-';
            if( $pluginAttributeName != '' ) {
                $inputAttributeNamePrefix .= $pluginAttributeName . '-';
            }
            foreach( $attribute[ $attributeID ] as $optionName => $optionValue ) {
                if( is_array( $optionValue ) ) {
                    $attributeValue = json_encode( $optionValue );
                } elseif( is_bool( $optionValue ) ) {
                    $attributeValue = $optionValue ? 'true' : 'false';
                } else {
                    $attributeValue = $optionValue;
                }
                $inputAttributeList[ $inputAttributeNamePrefix . str_replace( '_', '-', $optionName ) ] = $attributeValue;
            }
        }
    }

    public static function setDefaultValue( &$value, &$attribute, $isSubmitted )
    {
        if( !$isSubmitted && isset( $attribute[ 'value' ] ) ) {
            $value = $attribute[ 'value' ];
        }
    }

    public static function isItemActive( $itemValue, $itemAttributes, $itemActiveAttributeID, $value, $isSubmitted )
    {
        if( $isSubmitted ) {
            if( !is_null( $value ) ) {
                if( is_array( $value ) ) {
                    if( in_array( $itemValue, $value ) ) {
                        return true;
                    }
                } else {
                    if( $value == $itemValue ) {
                        return true;
                    }
                }
            }
        } else {
            if( isset( $itemAttributes[ $itemActiveAttributeID ] ) && $itemAttributes[ $itemActiveAttributeID ] ) {
                return true;
            }
        }
        return false;
    }

    public static function getSubInputValue( $index, $itemAttributes, $value, $isSubmitted )
    {
        if( $isSubmitted ) {
            if( isset( $value[ $index ] ) ) {
                return $value[ $index ];
            }
        } else {
            if( isset( $itemAttributes[ 'value' ] ) ) {
                return $itemAttributes[ 'value' ];
            }
        }
        return '';
    }

    public static function setJsAttributesByList( &$inputAttributeList, &$attribute, $attributeIDList )
    {
        foreach( $attributeIDList as $attributeID ) {
            if( !isset( $attribute[ $attributeID ] ) ) {
                continue;
            }
            if( is_array( $attribute[ $attributeID ] ) ) {
                $attributeValue = json_encode( $attribute[ $attributeID ] );
            } elseif( is_bool( $attribute[ $attributeID ] ) ) {
                $attributeValue = $attribute[ $attributeID ] ? 'true' : 'false';
            } else {
                $attributeValue = $attribute[ $attributeID ];
            }
            $inputAttributeList[ 'data-js-' . str_replace( '_', '-', $attributeID ) ] = $attributeValue;
        }
    }

    public static function setJsAttributesByPrefix( &$inputAttributeList, &$attribute )
    {
        foreach( array_keys( $attribute ) as $attributeID ) {
            if( substr( $attributeID, 0, 3 ) !== 'js_' ) {
                continue;
            }
            if( is_array( $attribute[ $attributeID ] ) ) {
                $attributeValue = json_encode( $attribute[ $attributeID ] );
            } elseif( is_bool( $attribute[ $attributeID ] ) ) {
                $attributeValue = $attribute[ $attributeID ] ? 'true' : 'false';
            } else {
                $attributeValue = $attribute[ $attributeID ];
            }
            $inputAttributeList[ 'data-' . str_replace( '_', '-', $attributeID ) ] = $attributeValue;
        }
    }

    public static function url( $path = false, $query = false )
    {
        if( isset( $_SERVER[ 'HTTPS' ] ) ){
            $url = ( $_SERVER[ 'HTTPS' ] && $_SERVER[ 'HTTPS' ] != 'off' ) ? 'https' : 'http';
        } else {
            $url = 'http';
        }
        $url .= '://' . $_SERVER[ 'HTTP_HOST' ];
        $url .= is_string( $path ) ? $path : ( defined( 'FORMSPLUS_REQUEST_URI' ) ? FORMSPLUS_REQUEST_URI : $_SERVER[ 'REQUEST_URI' ] );
        $url = strtok( $url, '?' );
        if( is_array( $query ) ) {
            $url .= '?' . http_build_query( $query );
        }
        return $url;
    }
}
