<?php

namespace FormsPlus\Framework\Helper\Attribute;

class ValueHandler
{
    public static function jsonEncode( &$value, &$stringValue, $attributeID, $postData, $postFiles, $framework )
    {
        $value = json_encode( $postData );
    }

    public static function notStore( &$value, &$stringValue, $attributeID, $postData, $postFiles, $framework )
    {
        $stringValue = '';
    }

    public static function passwordHash( &$value, &$stringValue, $attributeID, $postData, $postFiles, $framework )
    {
        $stringValue = function_exists( 'password_hash' ) ? password_hash( $postData, PASSWORD_DEFAULT, array( 'cost' => 12 ) ) : md5( $postData );
    }

    public static function randomHash( &$value, &$stringValue, $attributeID, $postData, $postFiles, $framework )
    {
        $stringValue = md5( uniqid( rand(), true ) );
    }
}
