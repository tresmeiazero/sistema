<?php

namespace FormsPlus\Framework\Helper;

class Validation
{
    public static function checkCreditCard( $value )
    {
        if( preg_match('/[^0-9 \-]+/', $value ) ) {
            return false;
        }
        $nCheck = 0;
        $bEven  = false;
        $value = preg_replace('/\D/', '', $value);
        if( strlen($value) < 13 || strlen($value) > 19 ) {
            return false;
        }
        for( $n = strlen($value) - 1; $n >= 0; $n-- ) {
            $cDigit = $value{$n};
            $nDigit = intval($cDigit, 10);
            if ($bEven) {
                if (($nDigit *= 2) > 9) {
                    $nDigit -= 9;
                }
            }
            $nCheck += $nDigit;
            $bEven = !$bEven;
        }
        return ($nCheck % 10) === 0;
    }

    public static function getCaptchaHash($value){
        $hash = 5381;
        $value = strtoupper($value);
        if( PHP_INT_SIZE == 8 ) {
            for($i = 0; $i < strlen($value); $i++) {
                $hash = (self::leftShift32($hash, 5) + $hash) + ord(substr($value, $i));
            }
        } else {
            for($i = 0; $i < strlen($value); $i++) {
                $hash = (($hash << 5) + $hash) + ord(substr($value, $i));
            }
        }
        return $hash;
    }

    public static function leftShift32($number, $steps) { 
        $binary = decbin($number);
        $binary = str_pad($binary, 32, "0", STR_PAD_LEFT);
        $binary = $binary.str_repeat("0", $steps);
        $binary = substr($binary, strlen($binary) - 32);
        return ($binary{0} == "0" ? bindec($binary) : 
            -(pow(2, 31) - bindec(substr($binary, 1)))); 
    }
}