<!DOCTYPE html>
<html lang="<?php echo $this->variableList[ 'locale' ]; ?>">
    <head>
        <title><?php echo $this->translateText( 'base.site.title' ); ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="<?php echo $this->variableList[ 'asset_directory_uri' ]; ?>font-awesome/css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="<?php echo $this->variableList[ 'asset_directory_uri' ]; ?>bootstrap/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="<?php echo $this->variableList[ 'asset_directory_uri' ]; ?>gantella/css/custom.css" rel="stylesheet">
    </head>
    <body style="background:#F7F7F7;">
        <?php echo $content; ?>
    </body>
</html>
