<?php
    $formConfigList = $this->storage->fetchLatestFormListPerConfig();
?>
<div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
        <a href="<?php echo $_SERVER[ 'PHP_SELF' ]; ?>" class="site_title"> <img src="images/LogoHorizontalBranco.png" class="img-responsive" width="50px" style="position: relative; top: 8px"><!-- <i class="fa fa-edit"></i> --><span><?php echo $this->translateText( 'base.site.shortTitle' ); ?> </span></a>
    </div>
    <div class="clearfix"></div>
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <ul class="nav side-menu">
                <li><a href="<?php echo $this->pageLink(); ?>"<?php echo ( $this->variableList[ 'content_info' ][ 'action' ] == 'formList' ) ? ' class="selected"' : ''; ?>><i class="fa fa-home"></i> <?php echo $this->translateText( 'base.sidebarMenu.home' ); ?></a></li>

                <li><a href="<?php echo $this->pageLink( array( 'action' => 'collectionSearch' ) ); ?>"<?php echo ( $this->variableList[ 'content_info' ][ 'action' ] == 'collectionSearch' ) ? ' class="selected"' : ''; ?>><i class="fa fa-search"></i> <?php echo $this->translateText( 'base.sidebarMenu.search' ); ?></a></li>
                <?php if( !empty( $formConfigList ) ) { ?>
                    <li><a><i class="fa fa-edit"></i> <?php echo $this->translateText( 'base.sidebarMenu.forms' ); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <?php foreach( $formConfigList as $formConfig ) { ?>
                                <li><a href="<?php echo $this->pageLink( array( 'action' => 'formView', 'formID' => $formConfig[ 'id' ] ) ); ?>"<?php echo ( isset( $this->variableList[ 'content_info' ][ 'config_file' ] ) && $this->variableList[ 'content_info' ][ 'config_file' ] == $formConfig[ 'config_file' ] ) ? ' class="selected"' : ''; ?>><?php echo $formConfig[ 'configuration' ][ 'name' ]; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
                  <li><a><i class="fa fa-list"></i> Landing Pages <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            
                                <li><a href="http://planosdesaude360.com.br/lp/intermedica" target="_blank">Intermedica</a></li>
                            
                        </ul>
                    </li>
                <?php if( count( $this->configuration[ 'locale' ][ 'admin_panel' ] ) > 1 ) { ?>
                    <li><a><i class="fa fa-language"></i> <?php echo $this->translateText( 'base.sidebarMenu.language' ); ?> <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <?php foreach( $this->configuration[ 'locale' ][ 'admin_panel' ] as $locale ) { ?>
                                <li><a href="<?php echo $this->pageLink( array( 'action' => 'languageSwitch', 'locale' => $locale, 'redirect' => $_GET ) ); ?>"><?php echo isset( $this->configuration[ 'locale' ][ 'name_list' ][ $locale ] ) ? $this->configuration[ 'locale' ][ 'name_list' ][ $locale ] : $locale; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
                <li><a href="<?php echo $this->pageLink( array( 'action' => 'systemInfo' ) ); ?>"<?php echo ( $this->variableList[ 'content_info' ][ 'action' ] == 'systemInfo' ) ? ' class="selected"' : ''; ?>><i class="fa fa-cogs"></i> <?php echo $this->translateText( 'base.sidebarMenu.system' ); ?></a></li>

                
            </ul>
        </div>
    </div>
</div>
