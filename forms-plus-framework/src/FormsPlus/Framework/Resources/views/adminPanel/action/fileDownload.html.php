<?php
    use FormsPlus\Framework\Helper\UnitFormat;
    if( isset( $_GET[ 'collectionID' ] ) && isset( $_GET[ 'attributeID' ] ) && isset( $_GET[ 'fileIndex' ] ) ) {
        $downloadFileInfo = $this->storage->getCollectionFileInfo( $_GET[ 'collectionID' ], $_GET[ 'attributeID' ], $_GET[ 'fileIndex' ] );
        if( empty( $downloadFileInfo ) ) {
            return array( 'error' => $this->translateText( 'base.fileDownload.alert.fileNotFoundInData' ) );
        }
        if( !file_exists( $downloadFileInfo[ 'real_path' ] ) ) {
            return array( 'error' => $this->translateText( 'base.fileDownload.alert.fileNotFoundInStorage' ) );
        }
        header( 'Content-type: ' . $downloadFileInfo[ 'type' ] );
        header( 'Content-Disposition: inline; filename="' . $downloadFileInfo[ 'name' ] . '"' );
        header( 'Content-Length: ' . $downloadFileInfo[ 'size' ] );
        @readfile( $downloadFileInfo[ 'real_path' ] );
        $this->terminateScript();
    }
    $collection = !empty( $_GET[ 'collectionID' ] ) ? $this->storage->fetchCollection( $_GET[ 'collectionID' ] ) : null;
    if( empty( $collection  ) ) {
        return array( 'error' => $this->translateText( 'base.collectionView.alert.noCollectionWithID' ) );
    }
    $fileAttribute = !empty( $_GET[ 'attributeID' ] ) && isset( $collection[ 'attribute_list' ][ $_GET[ 'attributeID' ] ][ 'files' ] ) ? $collection[ 'attribute_list' ][ $_GET[ 'attributeID' ] ][ 'files' ] : null;
    if( empty( $fileAttribute  ) ) {
        return array( 'error' => $this->translateText( 'base.fileDownload.alert.noFileAttributeWithID' ) );
    }
    $fileAttributeID = $_GET[ 'attributeID' ];
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo $collection[ 'form_configuration' ][ 'name' ]; ?> <small><?php echo $this->translateText( 'base.collectionView.heading', array( '%id%' => $collection[ 'collection_id' ] ) ); ?></small></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo $this->translateText( 'base.fileDownload.panel.name' ); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content rm-mb-last">
        <table class="table" style="width: auto;">
            <thead>
                <tr>
                    <th class="text-center heading" colspan="4"><big><?php echo $this->translateText( 'base.fileDownload.table.fileList' ); ?></big></th>
                </tr>
                <tr>
                    <th><?php echo $this->translateText( 'base.fileDownload.column.name' ); ?></th>
                    <th><?php echo $this->translateText( 'base.fileDownload.column.size' ); ?></th>
                    <th><?php echo $this->translateText( 'base.fileDownload.column.type' ); ?></th>
                    <th><?php echo $this->translateText( 'base.fileDownload.column.download' ); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach( $fileAttribute as $attributeFileIndex => $attributeFile ) { ?>
                    <?php $fileList = isset( $attributeFile[ 'name' ] ) ? array( $attributeFile ) : $attributeFile; ?>
                    <?php foreach( $fileList as $index => $file ) { ?>
                        <tr>
                            <td><?php echo $file[ 'name' ]; ?></td>
                            <td><?php echo UnitFormat::humanFilesize( $file[ 'size' ] ); ?></td>
                            <td><?php echo $file[ 'type' ]; ?></td>
                            <td class="text-center"><a href="<?php echo $this->pageLink( array( 'action' => 'fileDownload', 'collectionID' =>  $collection[ 'collection_id' ], 'attributeID' => $fileAttributeID, 'fileIndex' => ( !isset( $attributeFile[ 'name' ] ) ) ? array( $attributeFileIndex, $index ) : array( $attributeFileIndex ) ) ); ?>"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
