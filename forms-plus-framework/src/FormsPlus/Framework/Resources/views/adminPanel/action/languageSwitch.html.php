<?php
    if( empty( $_GET[ 'locale' ] ) || !in_array( $_GET[ 'locale' ], $this->configuration[ 'locale' ][ 'admin_panel' ] ) ) {
        return array( 'error' => $this->translateText( 'base.languageSwitch.alert.noLocaleWithID' ) );
    }
    $this->session->getSegment( 'FormsPlus\AdminPanel' )->set( 'locale', $_GET[ 'locale' ] );
    $redirect = !empty( $_GET[ 'redirect' ] ) ? $_GET[ 'redirect' ] : array();
    $this->pageRedirect( $redirect );
?>
