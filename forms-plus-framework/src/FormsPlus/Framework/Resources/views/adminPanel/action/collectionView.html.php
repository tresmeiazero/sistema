<?php
    $collection = !empty( $_GET[ 'collectionID' ] ) ? $this->storage->fetchCollection( $_GET[ 'collectionID' ] ) : null;
    if( empty( $collection  ) ) {
        return array( 'error' => $this->translateText( 'base.collectionView.alert.noCollectionWithID' ) );
    }
    $form = $this->storage->fetchForm( $collection[ 'form_id' ] );
    $this->variableList[ 'content_info' ][ 'config_file' ] = $form[ 'config_file' ];
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo $collection[ 'form_configuration' ][ 'name' ]; ?> <small><?php echo $this->translateText( 'base.collectionView.heading', array( '%id%' => $collection[ 'collection_id' ] ) ); ?></small></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="x_panel">
    <div class="x_title">
        <h2><?php echo $this->translateText( 'base.collectionView.panel.name' ); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content rm-mb-last">
        <table class="table" style="width: auto;">
            <thead>
                <tr>
                    <th class="text-center heading" colspan="2"><big><?php echo $this->translateText( 'base.collectionView.table.formAttributes' ); ?></big></th>
                </tr>
                <tr>
                    <th><?php echo $this->translateText( 'base.collectionView.column.name' ); ?></th>
                    <th><?php echo $this->translateText( 'base.collectionView.column.value' ); ?></th>
                </tr>
            </thead>
            <tbody>
              <tr>
                  <th><?php echo $this->translateText( 'base.collectionList.column.submitted' ); ?></th>
                  <td><?php echo $collection[ 'collection_created' ]; ?></td>
              </tr>
              <tr>
                  <th><?php echo $this->translateText( 'base.collectionList.column.ip' ); ?></th>
                  <td><?php echo $collection[ 'collection_ip' ]; ?></td>
              </tr>
              <?php foreach( $collection[ 'attribute_list' ] as $attributeID => $attributeValue ) { ?>
                  <tr>
                      <th><?php echo $collection[ 'form_configuration' ][ 'attribute_list' ][ $attributeID ][ 'name' ]; ?></th>
                      <td>
                          <?php
                              if( isset( $attributeValue[ 'files' ] ) ) {
                                  echo $this->generateFileAttributeHtml( $collection[ 'collection_id' ], $attributeID, $attributeValue[ 'files' ] );
                              } else {
                                  echo htmlspecialchars( $attributeValue[ 'final_value' ] );
                              }
                          ?>
                      </td>
                  </tr>
              <?php } ?>
            </tbody>
        </table>
        <?php if( !empty( $collection[ 'form_configuration' ][ 'cloning_attribute_group_list' ] ) ) { ?>
            <?php foreach( $collection[ 'form_configuration' ][ 'cloning_attribute_group_list' ] as $groupName => $attributeList ) { ?>
                <?php
                    $indexList = array();
                    foreach( $attributeList as $attributeID ) {
                        if( isset( $collection[ 'attribute_list' ][ $attributeID ][ 'files' ] ) ) {
                            $indexList = array_merge( $indexList, array_keys( $collection[ 'attribute_list' ][ $attributeID ][ 'files' ] ) );
                        } elseif( isset( $collection[ 'attribute_list' ][ $attributeID ][ 'string_value' ] ) ) {
                            $indexList = array_merge( $indexList, array_keys( $collection[ 'attribute_list' ][ $attributeID ][ 'string_value' ] ) );
                        }
                    }
                    $indexList = array_unique( $indexList );
                    sort( $indexList );
                ?>
                <table class="table" style="width: auto;">
                    <thead>
                        <tr>
                            <th class="text-center heading" colspan="<?php echo count( $attributeList ) + 1; ?>"><big><?php echo $groupName; ?></big></th>
                        </tr>
                        <tr>
                            <th>#</th>
                            <?php foreach( $attributeList as $attributeID ) { ?>
                                <?php if( !isset( $collection[ 'form_configuration' ][ 'attribute_list' ][ $attributeID ] ) ) continue; ?>
                                <th><?php echo $collection[ 'form_configuration' ][ 'attribute_list' ][ $attributeID ][ 'name' ]; ?></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $counter = 0; ?>
                    <?php foreach( $indexList as $index ) { ?>
                        <?php $counter++; ?>
                        <tr>
                            <th scope="row"><?php echo $counter; ?></th>
                            <?php foreach( $attributeList as $attributeID ) { ?>
                                <?php if( !isset( $collection[ 'form_configuration' ][ 'attribute_list' ][ $attributeID ] ) ) continue; ?>
                                <td>
                                    <?php
                                        if( isset( $collection[ 'attribute_list' ][ $attributeID ][ 'files' ][ $index ] ) ) {
                                            echo $this->generateFileAttributeHtml( $collection[ 'collection_id' ], $attributeID, $collection[ 'attribute_list' ][ $attributeID ][ 'files' ][ $index ], $index );
                                        } elseif( isset( $collection[ 'attribute_list' ][ $attributeID ][ 'string_value' ][ $index ] ) ) {
                                            echo htmlspecialchars( $collection[ 'attribute_list' ][ $attributeID ][ 'string_value' ][ $index ] );
                                        } else {
                                            echo '-';
                                        }
                                    ?>
                                </td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<div class="x_buttons">
    <a href="<?php echo $this->pageLink( array( 'action' => 'formView', 'formID' => $collection[ 'form_id' ] ) ); ?>" class="btn btn-primary btn-sm"><i class="fa fa-chevron-left"></i> <?php echo $this->translateText( 'base.collectionView.button.backToList' ); ?></a>
    <?php if( isset( $this->configuration[ 'administration' ][ 'permit_editing' ] ) && $this->configuration[ 'administration' ][ 'permit_editing' ] ) { ?>
        <a href="<?php echo $this->pageLink( array( 'action' => 'collectionEdit', 'collectionID' => $collection[ 'collection_id' ] ) ); ?>" class="btn btn-success btn-sm"><i class="fa fa-pencil-square-o"></i> <?php echo $this->translateText( 'base.collectionView.button.editCollection' ); ?></a>
    <?php } ?>
</div>
