<?php
    $this->variableList[ 'style_list' ][ 301 ] = 'gantella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css';
    $this->variableList[ 'style_list' ][ 302 ] = 'gantella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css';
    $this->variableList[ 'script_list' ][ 301 ] = 'gantella/vendors/datatables.net/js/jquery.dataTables.min.js';
    $this->variableList[ 'script_list' ][ 302 ] = 'gantella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js';
    $this->variableList[ 'script_list' ][ 303 ] = 'gantella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js';
    $this->variableList[ 'script_list' ][ 304 ] = 'gantella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js';
    $formConfigList = $this->storage->fetchLatestFormListPerConfig();
    $datatableTranslationList = array(
        'sZeroRecords'  => $this->translateText( 'base.datatable.sZeroRecords' ),
        'sInfo'         => $this->translateText( 'base.datatable.sInfo' ),
        'sInfoEmpty'    => $this->translateText( 'base.datatable.sInfoEmpty' ),
        'sInfoFiltered' => $this->translateText( 'base.datatable.sInfoFiltered' ),
        'sLengthMenu'   => $this->translateText( 'base.datatable.sLengthMenu' ),
        'sSearch'       => $this->translateText( 'base.datatable.sSearch' ),
        'paginate'      => array(
            'next'          => $this->translateText( 'base.datatable.paginate.next' ),
            'previous'      => $this->translateText( 'base.datatable.paginate.previous' )
        )
    );
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo $this->translateText( 'base.formList.heading' ); ?></h3>
    </div>
</div>
<div class="x_panel">
    <div class="x_content">
        <?php if( empty( $formConfigList ) ) { ?>
            <div class="alert alert-info rm-mr" role="alert">
                <?php echo $this->translateText( 'base.formList.alert.noCollectedData' ); ?>
            </div>
        <?php } else { ?>
            <table data-plugin-datatable="<?php echo htmlspecialchars( json_encode( $datatableTranslationList ), ENT_QUOTES, 'UTF-8' ); ?>" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                        <th><?php echo $this->translateText( 'base.formList.column.formName' ); ?></th>
                        <th><?php echo $this->translateText( 'base.formList.column.created' ); ?></th>
                        <th><?php echo $this->translateText( 'base.formList.column.config' ); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach( $formConfigList as $formConfig ) { ?>
                        <tr>
                            <td><a href="<?php echo $this->pageLink( array( 'action' => 'formView', 'formID' => $formConfig[ 'id' ] ) ); ?>"><?php echo $formConfig[ 'configuration' ][ 'name' ]; ?></a></td>
                            <td><?php echo $formConfig[ 'created' ]; ?></td>
                            <td><?php echo $formConfig[ 'config_file' ]; ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        <?php } ?>
    </div>
</div>
