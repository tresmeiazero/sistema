<?php
    $form = !empty( $_GET[ 'formID' ] ) ? $this->storage->fetchForm( $_GET[ 'formID' ] ) : null;
    if( empty( $form  ) ) {
        return array( 'error' => $this->translateText( 'base.formView.alert.noFormWithID' ) );
    }
    $formConfiguration = $form[ 'configuration' ];
    $collectionTableConfiguration = $form[ 'preference_configuration' ][ 'administration' ][ 'collection_table' ];
    if( isset( $_POST[ 'applyPreferencesAction' ] ) && isset( $_POST[ 'defaultSortField' ] ) && isset( $_POST[ 'defaultSortOrder' ] ) ) {
        $preference = array();
        $preference[ 'administration' ][ 'collection_table' ][ 'default_sort_field' ] = $_POST[ 'defaultSortField' ];
        $preference[ 'administration' ][ 'collection_table' ][ 'default_sort_order' ] = !empty( $_POST[ 'defaultSortOrder' ] );
        $preference[ 'administration' ][ 'collection_table' ][ 'field_presence' ][ 'property_list' ] = array();
        $preference[ 'administration' ][ 'collection_table' ][ 'field_presence' ][ 'attribute_list' ] = array();
        if( isset( $_POST[ 'showFields' ] ) && is_array( $_POST[ 'showFields' ] ) ) {
            if( !in_array( $_POST[ 'defaultSortField' ], $_POST[ 'showFields' ] ) ) {
                $preference[ 'administration' ][ 'collection_table' ][ 'default_sort_field' ] = $_POST[ 'showFields' ][ 0 ];
            }
            foreach( $_POST[ 'showFields' ] as $showField ) {
                $fieldType = substr( $showField, 0, 4 );
                if( $fieldType == 'prop' ) {
                    $preference[ 'administration' ][ 'collection_table' ][ 'field_presence' ][ 'property_list' ][] = substr( $showField, 5 );
                } elseif( $fieldType == 'attr' ) {
                    $preference[ 'administration' ][ 'collection_table' ][ 'field_presence' ][ 'attribute_list' ][] = substr( $showField, 5 );
                }
            }
        }
        $this->storage->updateFormPreferenceConfiguration( $_GET[ 'formID' ], $preference );
        $this->pageRedirect( array( 'action' => 'formView', 'formID' => $_GET[ 'formID' ] ) );
    }
?>
<div class="page-title">
    <div class="title_left">
        <h3><?php echo $formConfiguration[ 'name' ]; ?></h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="x_panel">
    <div class="x_title">
        <h2><i class="fa fa-cog"></i> <?php echo $this->translateText( 'base.formPreferences.panel.name' ); ?></h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <form method="post">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label><?php echo $this->translateText( 'base.formPreferences.label.defaultSortField' ); ?>:</label>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="defaultSortField" value="prop_collection_created"<?php echo ( $collectionTableConfiguration[ 'default_sort_field' ] == 'prop_collection_created' ) ? ' checked' : ''; ?>> Submitted
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="defaultSortField" value="prop_collection_ip"<?php echo ( $collectionTableConfiguration[ 'default_sort_field' ] == 'prop_collection_ip' ) ? ' checked' : ''; ?>> IP
                                    </label>
                                </div>
                                <?php
                                    $i = 2;
                                    $rowCount = ceil( count( $formConfiguration[ 'attribute_list' ] ) / 2 ) + 1;
                                ?>
                                <?php foreach( $formConfiguration[ 'attribute_list' ] as $attributeID => $attribute ) { ?>
                                    <?php if( $i % $rowCount === 0 ) { ?>
                                        </div><div class="col-sm-6">
                                    <?php } $i++; ?>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="defaultSortField" value="attr_<?php echo $attributeID; ?>"<?php echo ( $collectionTableConfiguration[ 'default_sort_field' ] == 'attr_' . $attributeID ) ? ' checked' : ''; ?>> <?php echo $attribute[ 'name' ]; ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><?php echo $this->translateText( 'base.formPreferences.label.defaultSortOrder' ); ?>:</label>
                        <label class="radio-inline">
                            <input type="radio" name="defaultSortOrder" value="1"<?php echo $collectionTableConfiguration[ 'default_sort_order' ] ? ' checked' : ''; ?>> <?php echo $this->translateText( 'base.formPreferences.label.ascending' ); ?>
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="defaultSortOrder" value="0"<?php echo $collectionTableConfiguration[ 'default_sort_order' ] ? '' : ' checked'; ?>> <?php echo $this->translateText( 'base.formPreferences.label.descending' ); ?>
                        </label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Show columns:</label>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="showFields[]" value="prop_collection_created"<?php echo in_array( 'collection_created', $collectionTableConfiguration[ 'field_presence' ][ 'property_list' ] ) ? ' checked' : ''; ?>> Submitted
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="showFields[]" value="prop_collection_ip"<?php echo in_array( 'collection_ip', $collectionTableConfiguration[ 'field_presence' ][ 'property_list' ] ) ? ' checked' : ''; ?>> IP
                                    </label>
                                </div>
                                <?php
                                    $i = 2;
                                    $rowCount = ceil( count( $formConfiguration[ 'attribute_list' ] ) / 2 ) + 1;
                                ?>
                                <?php foreach( $formConfiguration[ 'attribute_list' ] as $attributeID => $attribute ) { ?>
                                    <?php if( $i % $rowCount === 0 ) { ?>
                                        </div><div class="col-sm-6">
                                    <?php } $i++; ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="showFields[]" value="attr_<?php echo $attributeID; ?>"<?php echo in_array( $attributeID, $collectionTableConfiguration[ 'field_presence' ][ 'attribute_list' ] ) ? ' checked' : ''; ?>> <?php echo $attribute[ 'name' ]; ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center-xs text-left-sm">
                <button name="applyPreferencesAction" class="btn btn-success btn-sm" type="submit"><?php echo $this->translateText( 'base.formPreferences.button.applyChanges' ); ?></button>
                <a class="btn btn-default btn-sm" href="<?php echo $this->pageLink( array( 'action' => 'formView', 'formID' => $form[ 'id' ] ) ); ?>"><?php echo $this->translateText( 'base.formPreferences.button.cancel' ); ?></a>
            </div>
        </form>
    </div>
</div>
