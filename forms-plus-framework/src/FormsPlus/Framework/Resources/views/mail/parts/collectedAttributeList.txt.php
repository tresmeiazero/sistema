<?php
    $groupAttributeList = !empty( $configuration[ 'cloning_attribute_group_list' ] ) ? array_unique( call_user_func_array( 'array_merge', $configuration[ 'cloning_attribute_group_list' ] ) ) : array();
    foreach( $data[ 'attribute_list' ] as $attributeID => $attribute ) {
        if( $attribute[ 'final_value' ] != '' && !( in_array( $attributeID, $groupAttributeList ) && is_array( $attribute[ 'string_value' ] ) ) && ( isset( $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) && $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) ) {
            echo $attribute[ 'name' ] . ': ' . $attribute[ 'final_value' ] . "\n";
        }
    }
    if( !empty( $configuration[ 'cloning_attribute_group_list' ] ) ) {
        echo "\n";
        foreach( $configuration[ 'cloning_attribute_group_list' ] as $groupName => $attributeList ) {
            if( !isset( $attributeList[ 0 ] ) || !isset( $data[ 'attribute_list' ][ $attributeList[ 0 ] ] ) || !is_array( $data[ 'attribute_list' ][ $attributeList[ 0 ] ][ 'string_value' ] ) ) continue;
            $indexList = array_keys( $data[ 'attribute_list' ][ $attributeList[ 0 ] ][ 'string_value' ] );
            echo $groupName . ":\n";
            foreach( $indexList as $index ) {
                foreach( $attributeList as $attributeID ) {
                    if( is_array( $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ] ) && isset( $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ][ $index ] ) && $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ][ $index ] != '' && ( isset( $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) && $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) ) {
                        echo '  ' . $data[ 'attribute_list' ][ $attributeID ][ 'name' ] . ': ' . $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ][ $index ] . "\n";
                    }
                }
                echo "\n";
            }
        }
    }
?>
