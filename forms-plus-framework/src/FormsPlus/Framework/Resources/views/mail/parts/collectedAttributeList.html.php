<?php $groupAttributeList = !empty( $configuration[ 'cloning_attribute_group_list' ] ) ? array_unique( call_user_func_array( 'array_merge', $configuration[ 'cloning_attribute_group_list' ] ) ) : array(); ?>
<?php foreach( $data[ 'attribute_list' ] as $attributeID => $attribute ) { ?>
    <?php if( $attribute[ 'final_value' ] != '' && !( in_array( $attributeID, $groupAttributeList ) && is_array( $attribute[ 'string_value' ] ) ) && ( isset( $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) && $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) ) { ?>
        <strong><?php echo $attribute[ 'name' ]; ?></strong>: <?php echo $attribute[ 'final_value' ]; ?><br>
    <?php } ?>
<?php } ?>
<?php if( !empty( $configuration[ 'cloning_attribute_group_list' ] ) ) { ?>
    <br>
    <?php foreach( $configuration[ 'cloning_attribute_group_list' ] as $groupName => $attributeList ) { ?>
        <?php if( !isset( $attributeList[ 0 ] ) || !isset( $data[ 'attribute_list' ][ $attributeList[ 0 ] ] ) || !is_array( $data[ 'attribute_list' ][ $attributeList[ 0 ] ][ 'string_value' ] ) ) continue; ?>
        <?php $indexList = array_keys( $data[ 'attribute_list' ][ $attributeList[ 0 ] ][ 'string_value' ] ); ?>
        <strong><?php echo $groupName; ?></strong>:
        <ul>
            <?php foreach( $indexList as $index ) { ?>
                <li>
                    <?php foreach( $attributeList as $attributeID ) { ?>
                        <?php if( is_array( $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ] ) && isset( $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ][ $index ] ) && $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ][ $index ] != '' && ( isset( $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) && $configuration[ 'attribute_list' ][ $attributeID ][ 'in_mail' ] ) ) { ?>
                            <strong><?php echo $data[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?></strong>: <?php echo $data[ 'attribute_list' ][ $attributeID ][ 'string_value' ][ $index ]; ?><br>
                        <?php } ?>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    <?php } ?>
<?php } ?>
