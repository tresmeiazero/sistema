<?php use FormsPlus\Framework\Helper\Template; ?>

Thank you for registering at <?php echo $_SERVER[ 'HTTP_HOST' ]; ?>.

Your account information:
<?php
    self::includeTemplate( 'mail/parts/collectedAttributeList.txt.php', array(
        'configuration' => $configuration,
        'data'          => $data
    ));
?>

<?php
    if( ( isset( $parameterList[ 'email_attribute_id' ] ) && isset( $data[ 'attribute_list' ][ $parameterList[ 'email_attribute_id' ] ][ 'final_value' ] ) ) && ( isset( $parameterList[ 'hash_key_attribute_id' ] ) && isset( $data[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ] ) ) ) {
        $query = array(
            'action' => 'user_activation',
            'email'  => $data[ 'attribute_list' ][ $parameterList[ 'email_attribute_id' ] ][ 'final_value' ],
            'key'    => $data[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ]
        );
        echo "Click the following URL to confirm your account: \n" . Template::url( false, $query );
    } else {
        echo 'The activation link cannot be generated, please contact the website administrator to confirm your account.';
    }
?>
