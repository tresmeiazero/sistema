<?php use FormsPlus\Framework\Helper\Template; ?>

We received a request to reset the password on your <?php echo $_SERVER[ 'HTTP_HOST' ]; ?> account.

Your account information:
<?php
    self::includeTemplate( 'mail/parts/collectedAttributeList.txt.php', array(
        'configuration' => $configuration,
        'data'          => $data
    ));
?>

<?php
    if( ( isset( $parameterList[ 'email_attribute_id' ] ) && isset( $data[ 'attribute_list' ][ $parameterList[ 'email_attribute_id' ] ][ 'final_value' ] ) ) && ( isset( $parameterList[ 'hash_key_attribute_id' ] ) && isset( $data[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ] ) ) ) {
        $query = array(
            'action' => 'user_newpassword',
            'email'  => $data[ 'attribute_list' ][ $parameterList[ 'email_attribute_id' ] ][ 'final_value' ],
            'key'    => $data[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ]
        );
        echo "Click here to get a new password: \n" . Template::url( false, $query );
    } else {
        echo 'A new password cannot be generated, please contact the website administrator.';
    }
?>
