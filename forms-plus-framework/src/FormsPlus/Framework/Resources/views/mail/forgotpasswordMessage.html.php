<?php use FormsPlus\Framework\Helper\Template; ?>

We received a request to reset the password on your <a href="<?php echo Template::url( '/' ); ?>"><?php echo $_SERVER[ 'HTTP_HOST' ]; ?></a> account.<br><br>
<i>Your account information:</i><br>
<?php
    self::includeTemplate( 'mail/parts/collectedAttributeList.html.php', array(
        'configuration' => $configuration,
        'data'          => $data
    ));
?>
<br>
<?php
    if( ( isset( $parameterList[ 'email_attribute_id' ] ) && isset( $data[ 'attribute_list' ][ $parameterList[ 'email_attribute_id' ] ][ 'final_value' ] ) ) && ( isset( $parameterList[ 'hash_key_attribute_id' ] ) && isset( $data[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ] ) ) ) {
        $query = array(
            'action' => 'user_newpassword',
            'email'  => $data[ 'attribute_list' ][ $parameterList[ 'email_attribute_id' ] ][ 'final_value' ],
            'key'    => $data[ 'attribute_list' ][ $parameterList[ 'hash_key_attribute_id' ] ][ 'final_value' ]
        );
        echo "Click here to get a new password: <br>" . Template::url( false, $query );
    } else {
        echo 'A new password cannot be generated, please contact the website administrator.';
    }
?>
