<?php
    use FormsPlus\Framework\Helper\Template;
    Template::setDefaultValue( $value, $attribute, $isSubmitted );
?>
<div class="form-group<?php echo ( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) ? ' p-field-disabled' : ''; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier ) ); ?>
    <div class="p-rating">
        <div class="pull-<?php echo $configuration[ 'rtl' ] ? 'right' : 'left'; ?>">
            <?php if( $attribute[ 'show_cancel' ] ) { ?>
                <input type="radio" id="<?php echo $clone[ 'id' ] . 'fake0' . $identifier; ?>" name="<?php echo $identifier . $clone[ 'name' ]; ?>" class="p-rating-cancel" value="" />
                <label for="<?php echo $clone[ 'id' ] . 'fake0' . $identifier; ?>" class="p-field-cancel"><i class="fa fa-times"></i></label>
            <?php } ?>
            <?php for( $counter = intval( $attribute[ 'dimension' ] ); $counter >= 1; $counter-- ) { ?>
                <input type="radio" id="<?php echo $clone[ 'id' ] . 'fake' . $counter . $identifier; ?>" name="<?php echo $identifier . $clone[ 'name' ]; ?>" value="<?php echo $counter; ?>"<?php echo ( $counter == $value ) ? ' checked' : ''; ?>/>
                <label for="<?php echo $clone[ 'id' ] . 'fake' . $counter . $identifier; ?>">
                    <?php if( !empty( $attribute[ 'active_icon' ] ) ) { ?>
                        <span class="p-rating-active-icon"><i class="<?php echo $attribute[ 'active_icon' ]; ?>"></i></span>
                    <?php } ?>
                    <span class="p-rating-icon"><i class="<?php echo $attribute[ 'icon' ]; ?>"></i></span>
                </label>
            <?php } ?>
        </div>
    </div>
</div>