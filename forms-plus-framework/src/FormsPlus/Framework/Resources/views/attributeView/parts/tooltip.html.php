<?php if( !empty( $attribute[ 'tooltip' ][ 'content' ] ) ) { ?>
    <?php $supportedPositionList = array( 'top', 'top-left', 'top-right', 'bottom', 'bottom-left', 'bottom-right' ); ?>
    <span class="p-tooltip<?php echo ( !empty( $attribute[ 'tooltip' ][ 'position' ] ) && in_array( $attribute[ 'tooltip' ][ 'position' ], $supportedPositionList ) ) ? ' p-tooltip-' . $attribute[ 'tooltip' ][ 'position' ] : ''; ?>">
        <span class="p-tooltip-content"><?php echo $attribute[ 'tooltip' ][ 'content' ]; ?></span>
    </span>
<?php } ?>