<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array();
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    if( isset( $attribute[ 'required' ] ) && $attribute[ 'required' ] ) {
        $inputAttributeList[ 'required' ] = true;
    }
    if( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) {
        $inputAttributeList[ 'disabled' ] = true;
    }
?>
<div class="form-group<?php echo ( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) ? ' p-field-disabled' : ''; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier ) ); ?>
    <div class="p-form-cg<?php echo ( isset( $attribute[ 'check_big_icons' ] ) && $attribute[ 'check_big_icons' ] ) ? ' p-check-big-icons' : ''; ?><?php echo ( isset( $attribute[ 'view' ] ) && in_array( $attribute[ 'view' ], array( 'cross-panel', 'panel', 'inline' ) ) ) ? ( ' pt-form-' . $attribute[ 'view' ] ) : ''; ?>">
        <?php foreach( $attribute[ 'radiobutton_list' ] as $index => $radiobutton ) { ?>
            <?php if( empty( $radiobutton[ 'name' ] ) ) continue; ?>
            <?php
                $itemLabelClassList = array();
                $itemInputAttributeList = $inputAttributeList;
                $checkInput = ( !empty( $radiobutton[ 'check_input' ] ) ) ? $radiobutton[ 'check_input' ] : false;
                Template::setJsAttributesByPrefix( $itemInputAttributeList, $radiobutton );
                if( $checkInput !== false ) {
                    $itemLabelClassList[] = 'p-check-input';
                    $itemInputAttributeList[ 'class' ] = 'p-check-input';
                }
                if( isset( $radiobutton[ 'disabled' ] ) && $radiobutton[ 'disabled' ] ) {
                    $itemLabelClassList[] = 'p-field-disabled';
                    $itemInputAttributeList[ 'disabled' ] = true;
                }
            ?>
            <div class="radio">
                <label<?php echo !empty( $itemLabelClassList ) ? ' class="' . implode( ' ', $itemLabelClassList ) . '"' : ''; ?>>
                    <input type="radio" name="<?php echo $identifier . $clone[ 'name' ]; ?>[check]" value="<?php echo $index; ?>"<?php echo Template::isItemActive( $index, $radiobutton, 'checked', isset( $value[ 'check' ] ) ? $value[ 'check' ] : null, $isSubmitted ) ? ' checked' : ''; ?><?php echo Template::generateHTMLAttributes( $itemInputAttributeList ); ?>/>
                    <?php if( !empty( $radiobutton[ 'active_check_icon' ] ) ) { ?>
                        <span class="p-check-active-icon">
                            <span class="p-check-middle"><i class="<?php echo $radiobutton[ 'active_check_icon' ]; ?>"></i></span>
                        </span>
                    <?php } ?>
                    <span class="p-check-icon">
                        <?php if( !empty( $radiobutton[ 'check_icon' ] ) ) { ?>
                            <span class="p-check-middle"><i class="<?php echo $radiobutton[ 'check_icon' ]; ?>"></i></span>
                        <?php } else { ?>
                            <span class="p-check-block"></span>
                        <?php } ?>
                    </span>
                    <?php if( $checkInput !== false ) { ?>
                        <span class="form-group">
                            <?php
                                $checkInputAttributeList = array( 
                                    'class' => 'form-control',
                                    'type'  => ( ( !empty( $checkInput[ 'type' ] ) ) ?  $checkInput[ 'type' ] : 'text' ),
                                    'name'  => $identifier . $clone[ 'name' ] . '[input][' . $index . ']',
                                    'value' => Template::getSubInputValue( $index, $checkInput, isset( $value[ 'input' ] ) ? $value[ 'input' ] : null, $isSubmitted )
                                );
                                Template::setInputBaseAttributes( $checkInputAttributeList, $checkInput );
                                self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => $checkInputAttributeList, 'attribute' => $checkInput ));
                            ?>
                        </span>
                    <?php } else { ?>
                        <span class="p-label"><?php echo $radiobutton[ 'name' ]; ?></span>
                    <?php } ?>
                </label>
            </div>
        <?php } ?>
    </div>
</div>