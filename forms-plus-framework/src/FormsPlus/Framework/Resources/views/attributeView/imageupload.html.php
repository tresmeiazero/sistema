<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array();
    Template::setInputBaseAttributes( $inputAttributeList, $attribute );
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    if( !empty( $attribute[ 'accept' ] ) ) {
        $inputAttributeList[ 'accept' ] = $attribute[ 'accept' ];
    }
?>
<div class="form-group">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier ) ); ?>
    <?php if( $attribute[ 'multiple' ] ) { ?>
        <div class="row">
            <?php for( $counter = intval( $attribute[ 'image_quantity' ] ); $counter >= 1; $counter-- ) { ?>
                <div class="<?php echo $attribute[ 'image_grid_class' ]; ?>">
                    <div class="p-field-group">
                        <div class="p-image-upload p-picture-solo" data-js-image-upload="">
                            <input type="file" name="<?php echo $identifier . $clone[ 'name' ]; ?>[]"<?php echo Template::generateHTMLAttributes( $inputAttributeList ); ?>/>
                            <?php self::includeTemplate( 'attributeView/parts/imageuploadPreview.html.php', array( 'attribute' => $attribute ) ); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="p-image-upload p-picture-solo" data-js-image-upload="">
            <input type="file" id="<?php echo $clone[ 'id' ] . $identifier; ?>" name="<?php echo $identifier . $clone[ 'name' ]; ?>"<?php echo Template::generateHTMLAttributes( $inputAttributeList ); ?>/>
            <?php self::includeTemplate( 'attributeView/parts/imageuploadPreview.html.php', array( 'attribute' => $attribute ) ); ?>
        </div>
    <?php } ?>
</div>