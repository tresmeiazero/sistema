<?php 
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array();
    Template::setInputBaseAttributes( $inputAttributeList, $attribute );
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    if( !empty( $attribute[ 'accept' ] ) ) {
        $inputAttributeList[ 'accept' ] = $attribute[ 'accept' ];
    }
    $fakeInputAttributeList = array( 'class' => 'form-control' );
    if( isset( $inputAttributeList[ 'placeholder' ] ) ) {
        $fakeInputAttributeList[ 'placeholder' ] = $inputAttributeList[ 'placeholder' ];
    }
?>
<div class="form-group<?php echo ( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) ? ' p-field-disabled' : ''; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier ) ); ?>
    <div class="p-file-wrap">
        <input type="file" id="<?php echo $clone[ 'id' ] . $identifier; ?>" name="<?php echo $identifier . $clone[ 'name' ]; ?>" onchange="document.getElementById('<?php echo $clone[ 'id' ] . 'fake' . $identifier; ?>').value = this.value;"<?php echo Template::generateHTMLAttributes( $inputAttributeList ); ?>/>
        <?php
            if( $attribute[ 'browse_button_side' ] == 'right' ) unset( $attribute[ 'addon' ] );
            if( $attribute[ 'browse_button_side' ] == 'left' ) unset( $attribute[ 'icon' ] );
            self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => array_merge( $fakeInputAttributeList, array(
                'type'      => 'text',
                'id'        => $clone[ 'id' ] . 'fake' . $identifier,
                'readonly'  => true
            )), 'attribute' => $attribute )); 
        ?>
    </div>
</div>