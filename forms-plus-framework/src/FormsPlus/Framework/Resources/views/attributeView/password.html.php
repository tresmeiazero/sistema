<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array( 'class' => 'form-control' );
    Template::setInputBaseAttributes( $inputAttributeList, $attribute );
    Template::setInputStringAttributes( $inputAttributeList, $attribute );
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    if( $attribute[ 'show_hide_toggled' ] ) {
        $inputType = 'text';
        $toggleType = 'password';
        $toggledIcon = 'fa fa-lock';
        $untoggledIcon = 'fa fa-unlock';
    } else {
        $inputType = 'password';
        $toggleType = 'text';
        $toggledIcon = 'fa fa-unlock';
        $untoggledIcon = 'fa fa-lock';
    }
?>
<div class="form-group<?php echo ( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) ? ' p-field-disabled' : ''; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier, 'class' => 'p-field-label' ) ); ?>
    <?php
        $inputGroup = array();
        if( $attribute[ 'show_hide_toggle' ] ) {
            $inputGroup[ 'js_addon_html' ] = '<span class="p-js-link p-js-full" data-js-toggle-type="' . $toggleType . '" data-js-toggle-content=\'<i class="' . $toggledIcon . '"></i>\'><i class="' . $untoggledIcon . '"></i></span>';
            unset( $attribute[ 'addon' ] );
        }
        self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => array_merge( $inputAttributeList, array(
            'type' => $inputType,
            'id'   => $clone[ 'id' ] . $identifier,
            'name' => $identifier . $clone[ 'name' ]
        )), 'attribute' => $attribute, 'inputGroup' => $inputGroup ));
    ?>
</div>