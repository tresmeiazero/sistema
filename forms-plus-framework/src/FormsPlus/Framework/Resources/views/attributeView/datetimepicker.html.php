<?php
    use FormsPlus\Framework\Helper\Template;
    $datetimepickerAttributeList = array();
    Template::setDefaultValue( $value, $attribute, $isSubmitted );
    Template::setPluginAttributes( $datetimepickerAttributeList, $attribute, 'datetimepicker_option_list', 'date' );
    $inputAttributeList = array();
    Template::setInputBaseAttributes( $inputAttributeList, $attribute );
    Template::setJsAttributesByPrefix( $inputAttributeList, $attribute );
    if( !is_null( $value ) ) $inputAttributeList[ 'value' ] = $value;
?>
<div class="form-group<?php echo ( $attribute[ 'background' ] ) ? '' : ' p-datetime-no-bg'; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier, 'class' => ( $attribute[ 'inline' ] ? null : 'p-field-label' ) ) ); ?>
    <?php if( $attribute[ 'inline' ] ) { ?>
        <div class="p-inline-datetime" data-js-inline-datetimepick="true"<?php echo Template::generateHTMLAttributes( $datetimepickerAttributeList ); ?>>
            <input type="hidden" id="<?php echo $clone[ 'id' ] . $identifier; ?>" name="<?php echo $identifier . $clone[ 'name' ]; ?>" data-js-datetimepick="true"<?php echo Template::generateHTMLAttributes( $inputAttributeList ); ?>/>
        </div>
    <?php } else { ?>
        <?php
            self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => array_merge( $inputAttributeList, $datetimepickerAttributeList, array(
                'type'                 => 'text',
                'id'                   => $clone[ 'id' ] . $identifier,
                'name'                 => $identifier . $clone[ 'name' ],
                'class'                => 'form-control',
                'data-js-datetimepick' => 'true'
            )), 'attribute' => $attribute ));
        ?>
    <?php } ?>
</div>