<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array( 'class' => 'form-control' );
    Template::setDefaultValue( $value, $attribute, $isSubmitted );
    Template::setPluginAttributes( $inputAttributeList, $attribute, 'spectrum_option_list' );
    if( !is_null( $value ) ) $inputAttributeList[ 'value' ] = $value;
?>
<div class="form-group<?php echo ( $attribute[ 'background' ] ) ? '' : ' p-colorpick-no-bg'; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier, 'class' => ( $attribute[ 'inline' ] ? null : 'p-field-label' ) ) ); ?>
    <?php if( $attribute[ 'inline' ] ) { ?>
        <input type="text" id="<?php echo $clone[ 'id' ] . $identifier; ?>" name="<?php echo $identifier . $clone[ 'name' ]; ?>" data-js-inline-colorpick="true"<?php echo Template::generateHTMLAttributes( $inputAttributeList ); ?>/>
    <?php } else { ?>
        <?php
            self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => array_merge( $inputAttributeList, array(
                'type'              => 'text',
                'id'                => $clone[ 'id' ] . $identifier,
                'name'              => $identifier . $clone[ 'name' ],
                'data-js-colorpick' => 'true'
            )), 'attribute' => $attribute ));
        ?>
    <?php } ?>
</div>