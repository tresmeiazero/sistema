<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array();
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    Template::setJsAttributesByPrefix( $inputAttributeList, $attribute );
    if( isset( $attribute[ 'required' ] ) && $attribute[ 'required' ] ) {
        $inputAttributeList[ 'required' ] = true;
    }
    $checkmarkHTML = '';
    if( !empty( $attribute[ 'checkmark' ][ 'type' ] ) ) {
        if( $attribute[ 'checkmark' ][ 'type' ] == 'point' ) {
            $checkmarkHTML = '<span class="p-check-point"></span>';
        } elseif( $attribute[ 'checkmark' ][ 'type' ] == 'button' ) {
            $checkmarkHTML = '<span class="p-check-icon"><span class="p-check-block"></span></span>';
        } elseif( $attribute[ 'checkmark' ][ 'type' ] == 'switch' ) {
            $checkmarkHTML = '<span class="p-switch-icon" data-checked="' . htmlentities( $attribute[ 'checkmark' ][ 'checked_text' ], ENT_QUOTES ) . '" data-unchecked="' . htmlentities( $attribute[ 'checkmark' ][ 'unchecked_text' ], ENT_QUOTES ) . '"></span>';
        }
    }
    $subOptionClass = $configuration[ 'feature_list' ][ 'sub_selection' ] ? ' p-sub-option' : '';
?>
<div class="form-group">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier ) ); ?>
    <div class="p-form-pictures">
        <div class="row p-sm-row">
            <?php foreach( $attribute[ 'image_list' ] as $index => $image ) { ?>
                <?php if( empty( $image[ 'image' ] ) ) continue; ?>
                <?php
                    $itemInputAttributeList = $inputAttributeList;
                    Template::setJsAttributesByPrefix( $itemInputAttributeList, $image );
                ?>
                <div class="p-col<?php echo ( !empty( $attribute[ 'image_grid_class' ] ) ) ? ( ' ' . $attribute[ 'image_grid_class' ] ) : ''; ?><?php echo $subOptionClass; ?>">
                    <div class="p-picture-pick <?php echo ( $attribute[ 'multiple' ] ) ? ' p-picture-checkbox' : ' p-picture-radio'; ?>">
                        <label class="p-picture-solo">
                            <input <?php echo ( $attribute[ 'multiple' ] ) ? 'type="checkbox" name="' . $identifier . $clone[ 'name' ] . '[]"' : 'type="radio" name="' . $identifier . $clone[ 'name' ] . '"'; ?> value="<?php echo $index; ?>"<?php echo Template::isItemActive( $index, $image, 'selected', $value, $isSubmitted ) ? ' checked' : ''; ?><?php echo Template::generateHTMLAttributes( $itemInputAttributeList ); ?>/>
                            <?php echo $checkmarkHTML; ?>
                            <span class="p-preview">
                                <img src="<?php echo htmlentities( $image[ 'image' ], ENT_QUOTES ); ?>">
                            </span>
                            <?php if( !empty( $image[ 'caption' ] ) ) { ?>
                                <span class="p-preview-name"><?php echo $image[ 'caption' ]; ?></span>
                            <?php } ?>
                        </label>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
