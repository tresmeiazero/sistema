<?php
    use FormsPlus\Framework\Helper\Template;
    $inputAttributeList = array();
    Template::setInputValidationAttributes( $inputAttributeList, $attribute );
    Template::setJsAttributesByPrefix( $inputAttributeList, $attribute );
    if( isset( $attribute[ 'required' ] ) && $attribute[ 'required' ] ) {
        $inputAttributeList[ 'required' ] = true;
    }
    if( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) {
        $inputAttributeList[ 'disabled' ] = true;
    }
?>
<div class="form-group<?php echo ( isset( $attribute[ 'disabled' ] ) && $attribute[ 'disabled' ] ) ? ' p-field-disabled' : ''; ?>">
    <?php self::includeTemplate( 'attributeView/parts/label.html.php', array( 'attribute' => $attribute, 'identifier' => $clone[ 'id' ] . $identifier ) ); ?>
    <?php if( empty( $attribute[ 'checkbox' ] ) ) { ?>
        <div class="p-form-cg<?php echo ( isset( $attribute[ 'view' ] ) && in_array( $attribute[ 'view' ], array( 'cross-panel', 'panel', 'inline' ) ) ) ? ( ' pt-form-' . $attribute[ 'view' ] ) : ''; ?>">
            <?php foreach( $attribute[ 'checkbox_list' ] as $index => $checkbox ) { ?>
                <?php if( empty( $checkbox[ 'name' ] ) ) continue; ?>
                <?php
                    $itemLabelClassList = array();
                    $itemInputAttributeList = $inputAttributeList;
                    $checkInput = ( !empty( $checkbox[ 'check_input' ] ) ) ? $checkbox[ 'check_input' ] : false;
                    Template::setJsAttributesByPrefix( $itemInputAttributeList, $checkbox );
                    if( $checkInput !== false ) {
                        $itemInputAttributeList[ 'class' ] = 'p-check-input';
                        $itemLabelClassList[] = 'p-check-input';
                    }
                    if( isset( $checkbox[ 'disabled' ] ) && $checkbox[ 'disabled' ] ) {
                        $itemLabelClassList[] = 'p-field-disabled';
                        $itemInputAttributeList[ 'disabled' ] = true;
                    }
                ?>
                <div class="checkbox">
                    <label<?php echo !empty( $itemLabelClassList ) ? ' class="' . implode( ' ', $itemLabelClassList ) . '"' : ''; ?>>
                        <input type="checkbox" name="<?php echo $identifier . $clone[ 'name' ]; ?>[check][]" value="<?php echo $index; ?>"<?php echo Template::isItemActive( $index, $checkbox, 'checked', isset( $value[ 'check' ] ) ? $value[ 'check' ] : null, $isSubmitted ) ? ' checked' : ''; ?><?php echo Template::generateHTMLAttributes( $itemInputAttributeList ); ?>/>
                        <?php if( !empty( $checkbox[ 'active_check_icon' ] ) ) { ?>
                            <span class="p-check-active-icon">
                                <span class="p-check-middle"><i class="<?php echo $checkbox[ 'active_check_icon' ]; ?>"></i></span>
                            </span>
                        <?php } ?>
                        <span class="p-check-icon">
                            <?php if( !empty( $checkbox[ 'check_icon' ] ) ) { ?>
                                <span class="p-check-middle"><i class="<?php echo $checkbox[ 'check_icon' ]; ?>"></i></span>
                            <?php } else { ?>
                                <span class="p-check-block"></span>
                            <?php } ?>
                        </span>
                        <?php if( $checkInput !== false ) { ?>
                            <span class="form-group">
                                <?php
                                    $checkInputAttributeList = array( 
                                        'class' => 'form-control',
                                        'type'  => ( ( !empty( $checkInput[ 'type' ] ) ) ?  $checkInput[ 'type' ] : 'text' ),
                                        'name'  => $identifier . $clone[ 'name' ] . '[input][' . $index . ']',
                                        'value' => Template::getSubInputValue( $index, $checkInput, isset( $value[ 'input' ] ) ? $value[ 'input' ] : null, $isSubmitted )
                                    );
                                    Template::setInputBaseAttributes( $checkInputAttributeList, $checkInput );
                                    self::includeTemplate( 'attributeView/parts/inputGroup.html.php', array( 'input' => $checkInputAttributeList, 'attribute' => $checkInput ));
                                ?>
                            </span>
                        <?php } else { ?>
                            <span class="p-label"><?php echo $checkbox[ 'name' ]; ?></span>
                        <?php } ?>
                    </label>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="checkbox">
            <label>
                <input type="checkbox" id="<?php echo $clone[ 'id' ] . $identifier; ?>" name="<?php echo $identifier . $clone[ 'name' ]; ?>"<?php echo Template::isItemActive( true, $attribute, 'checked', $value, $isSubmitted ) ? ' checked' : ''; ?><?php echo Template::generateHTMLAttributes( $inputAttributeList ); ?>/>
                <span class="p-check-icon">
                    <span class="p-check-block"></span>
                </span>
                <span class="p-label"><?php echo $attribute[ 'checkbox' ]; ?></span>
            </label>
        </div>
    <?php } ?>
</div>