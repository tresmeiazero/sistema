<?php
/*
  Name: Forms Plus: PHP - Forms Framework
  URI: https://codecanyon.net/item/forms-plus-php-forms-framework/16155459
  Author: Swebdeveloper
  Version: 1.2.1
  Author URI: http://codecanyon.net/user/swebdeveloper
*/

namespace FormsPlus\Framework;

use FormsPlus\Framework\Helper\Storage;
use FormsPlus\Framework\Helper\Debug;
use FormsPlus\Framework\Helper\Mail;
use FormsPlus\Framework\Helper\Configuration;
use FormsPlus\Framework\Helper\Validation;
use FormsPlus\Framework\Helper\DummyFramework;

class FormsPlusFramework
{
    private $formID;
    private $configFile;
    private $configuration;
    private $storage;
    private $data = array( 'attribute_list' => array(), 'file_list' => array() );
    private $errorList = array();
    private $variableList = array(
        'is_form_id_output'    => false,
        'attribute_hash'       => '',
        'attributes_left'      => array(),
        'validation_rule_data' => array(),
        'template_path_list'   => array()
    );

    private $attributeList = array();
    private $isSubmitted = false;
    private $isValid = false;

    private static $styles = array( 'font-awesome', 'bootstrap' );
    private static $scripts = array();
    private static $staticVariableList = array(
        'asset_directory_uri'            => './',
        'application_root_directory'     => null,
        'available_style_list'           => array(),
        'available_script_list'          => array(),
        'used_attribute_identifier_list' => array(),
        'used_showblock_identifier_list' => array()
    );

    public function __construct( $configFile, $optionList = array() )
    {
        $this->formID = md5( $configFile );
        $this->configFile = $configFile;
        // add configuration from variable, it has higher priority than a config file
        if( !empty( $optionList[ 'configuration' ] ) ) {
            $this->variableList[ 'configuration' ] = $optionList[ 'configuration' ];
        }
        // generate configuration from the config file
        $configuration = new Configuration( $this );
        $this->configuration = $configuration->getConfig();
        // if the configuration is empty, the config file was not loaded
        if( empty( $this->configuration ) ) {
            return;
        }
        if( isset( $optionList[ 'asset_directory_uri' ] ) ) {
            self::$staticVariableList[ 'asset_directory_uri' ] = strval( $optionList[ 'asset_directory_uri' ] );
        }
        if( !empty( $this->configuration[ 'attribute_list' ] ) ) {
            $attributeIdentifierList = array_keys( $this->configuration[ 'attribute_list' ] );
            $this->variableList[ 'attributes_left' ] = $attributeIdentifierList;
            $this->attributeList = $attributeIdentifierList;
            $sortedAttributeList = $this->attributeList;
            sort( $sortedAttributeList );
            $this->variableList[ 'attribute_hash' ] = implode( ' ', $sortedAttributeList );
            self::$staticVariableList[ 'used_attribute_identifier_list' ] = array_merge( self::$staticVariableList[ 'used_attribute_identifier_list' ], array_fill_keys( $attributeIdentifierList, $this->configFile ) );
        } else {
            Debug::errorMessage( 'There are no attributes to build the form', $this->configFile );
        }
        if( $_SERVER[ 'REQUEST_METHOD' ] === 'POST' && isset( $_POST[ 'formid' ] ) && $_POST[ 'formid' ] == $this->formID ) {
            $this->isSubmitted = true;
            $this->getData();
            $this->validateData();
            if( empty( $this->errorList ) ) {
                $this->isValid = true;
                $mail = new Mail( $this );
                $mail->sendMail();
                $this->initStorage();
                if( $this->storage ) {
                    $collectionID = $this->storage->storeData();
                    if( $collectionID ) {
                        $this->data[ 'collection_id' ] = $collectionID;
                    }
                }
            }
        }
        $this->addRequiredStylesAndScripts();
    }

    public function __get( $name )
    {
        return isset( $this->$name ) ? $this->$name : null;
    }

    public static function getStaticVar( $name = null )
    {
        return self::$staticVariableList[ $name ];
    }

    public static function initDummy( $configFile )
    {
        return new DummyFramework( $configFile );
    }

    private function addRequiredStylesAndScripts()
    {
        $designIdentifier = strtolower( $this->configuration[ 'design' ][ 'name' ] );
        $colorIdentifier = strtolower( $this->configuration[ 'design' ][ 'color' ] );
        if( isset( $this->configuration[ 'design' ][ 'required_style_list' ] ) ) {
            foreach( $this->configuration[ 'design' ][ 'required_style_list' ] as $styleIdentifier ) {
                if( !isset( self::$staticVariableList[ 'available_style_list' ][ $styleIdentifier ] ) ) {
                    self::$staticVariableList[ 'available_style_list' ][ $styleIdentifier ] = $this->configuration[ 'design' ][ 'available_style_list' ][ $styleIdentifier ];
                }
            }
        }
        if( !isset( self::$staticVariableList[ 'available_style_list' ][ 'forms-plus-' . $designIdentifier ] ) ) {
            self::$staticVariableList[ 'available_style_list' ][ 'forms-plus-' . $designIdentifier ] = 'css/forms-plus-' . $this->configuration[ 'design' ][ 'name' ] . '.css';
        }
        if( !isset( self::$staticVariableList[ 'available_style_list' ][ 'forms-plus-' . $designIdentifier . '-rtl' ] ) ) {
            self::$staticVariableList[ 'available_style_list' ][ 'forms-plus-' . $designIdentifier . '-rtl' ] = 'css/forms-plus-' . $this->configuration[ 'design' ][ 'name' ] . '-rtl.css';
        }
        if( !isset( self::$staticVariableList[ 'available_style_list' ][ 'forms-plus-color-' . $designIdentifier . '-' . $colorIdentifier ] ) ) {
            self::$staticVariableList[ 'available_style_list' ][ 'forms-plus-color-' . $designIdentifier . '-' . $colorIdentifier ] = 'css/forms-plus-color/' . $this->configuration[ 'design' ][ 'name' ] . '/forms-plus-' . $this->configuration[ 'design' ][ 'color' ] . '.css';
        }
        if( !empty( $this->configuration[ 'gautocomplete' ][ 'api_key' ] ) ) {
            self::$staticVariableList[ 'available_script_list' ][ 'googleapis-maps' ] = 'https://maps.googleapis.com/maps/api/js?key=' . $this->configuration[ 'gautocomplete' ][ 'api_key' ] . '&libraries=places&language=en';
        }
        if( !empty( $this->configuration[ 'locale' ][ 'datetimepicker' ] ) ) {
            if( !isset( self::$staticVariableList[ 'available_script_list' ][ 'moment-locale' ] ) ) {
                self::$staticVariableList[ 'available_script_list' ][ 'moment-locale' ] = array();
            }
            foreach( $this->configuration[ 'locale' ][ 'datetimepicker' ] as $locale ) {
                self::$staticVariableList[ 'available_script_list' ][ 'moment-locale' ][] = 'js/moment/locale/' . $locale . '.js';
            }
            self::$staticVariableList[ 'available_script_list' ][ 'moment-locale' ] = array_unique( self::$staticVariableList[ 'available_script_list' ][ 'moment-locale' ] );
        }
        if( !empty( $this->configuration[ 'locale' ][ 'validation' ] ) ) {
            if( !isset( self::$staticVariableList[ 'available_script_list' ][ 'jquery-validation-localization' ] ) ) {
                self::$staticVariableList[ 'available_script_list' ][ 'jquery-validation-localization' ] = array();
            }
            foreach( $this->configuration[ 'locale' ][ 'validation' ] as $locale ) {
                self::$staticVariableList[ 'available_script_list' ][ 'jquery-validation-localization' ][] = 'js/jquery-validation/localization/messages_' . $locale . '.js';
            }
            self::$staticVariableList[ 'available_script_list' ][ 'jquery-validation-localization' ] = array_unique( self::$staticVariableList[ 'available_script_list' ][ 'jquery-validation-localization' ] );
        }
        if( isset( $this->configuration[ 'design' ][ 'required_style_list' ] ) ) {
            $missingStyleIdentifierList = array_diff( $this->configuration[ 'design' ][ 'required_style_list' ], self::$styles );
            if( !empty( $missingStyleIdentifierList ) ) {
                self::$styles = array_merge( self::$styles, $missingStyleIdentifierList );
            }
            unset( $missingStyleIdentifierList );
        }
        if( !in_array( 'forms-plus-' . $designIdentifier, self::$styles ) ) {
            self::$styles[] = 'forms-plus-' . $designIdentifier;
        }
        if( !in_array( 'forms-plus-color-' . $designIdentifier . '-' . $colorIdentifier, self::$styles ) ) {
            self::$styles[] = 'forms-plus-color-' . $designIdentifier . '-' . $colorIdentifier;
        }
        if( $this->configuration[ 'rtl' ] ) {
            if( !in_array( 'bootstrap-rtl', self::$styles ) ) {
                self::$styles[] = 'bootstrap-rtl';
            }
            if( !in_array( 'forms-plus-' . $designIdentifier . '-rtl', self::$styles ) ) {
                self::$styles[] = 'forms-plus-' . $designIdentifier . '-rtl';
            }
        }
        if( !empty( $this->configuration[ 'conditional_required_scripts_styles' ] ) ) {
            foreach( $this->configuration[ 'conditional_required_scripts_styles' ] as $conditionalRequired ) {
                if( $this->checkIfConditionPasses( $conditionalRequired[ 'condition' ] ) ) {
                    if( !empty( $conditionalRequired[ 'scripts' ] ) ) {
                        self::$scripts = array_merge( self::$scripts, $conditionalRequired[ 'scripts' ] );
                    }
                    if( !empty( $conditionalRequired[ 'styles' ] ) ) {
                        self::$styles = array_merge( self::$styles, $conditionalRequired[ 'styles' ] );
                    }
                }
            }
        }
        if( !empty( $this->configuration[ 'attribute_list' ] ) ) {
            foreach( $this->configuration[ 'attribute_list' ] as $attributeID => $attribute ) {
                if( !empty( $attribute[ 'required_scripts' ] ) ) {
                    self::$scripts = array_merge( self::$scripts, $attribute[ 'required_scripts' ] );
                }
                if( !empty( $attribute[ 'required_styles' ] ) ) {
                    self::$styles = array_merge( self::$styles, $attribute[ 'required_styles' ] );
                }
                if( !empty( $attribute[ 'conditional_required_scripts_styles' ] ) ) {
                    foreach( $attribute[ 'conditional_required_scripts_styles' ] as $conditionalRequired ) {
                        if( is_array( $conditionalRequired[ 'condition' ] ) ) {
                            $attributeFullCondition = array();
                            foreach( $conditionalRequired[ 'condition' ] as $conditionItem ) {
                                $attributeFullCondition[] = 'attribute_list.' . $attributeID . '.' . $conditionItem;
                            }
                        } else {
                            $attributeFullCondition = 'attribute_list.' . $attributeID . '.' . $conditionalRequired[ 'condition' ];
                        }
                        if( $this->checkIfConditionPasses( $attributeFullCondition ) ) {
                            if( !empty( $conditionalRequired[ 'scripts' ] ) ) {
                                self::$scripts = array_merge( self::$scripts, $conditionalRequired[ 'scripts' ] );
                            }
                            if( !empty( $conditionalRequired[ 'styles' ] ) ) {
                                self::$styles = array_merge( self::$styles, $conditionalRequired[ 'styles' ] );
                            }
                        }
                    }
                }
            }
            self::$styles = array_unique( self::$styles );
            self::$scripts = array_unique( self::$scripts );
        }
    }

    private function checkIfConditionPasses( $condition )
    {
        $data = $this->configuration;
        $valid = false;
        if( is_array( $condition ) ) {
            foreach( $condition as $conditionItem ) {
                $conditionItemNestedVar = $this->getNestedVarInArray( $data, $conditionItem );
                $valid = !empty( $conditionItemNestedVar );
                if( !$valid ) break;
            }
        } else {
            $conditionNestedVar = $this->getNestedVarInArray( $data, $condition );
            $valid = !empty( $conditionNestedVar );
        }
        return $valid;
    }

    private function getNestedVarInArray( $context, $name )
    {
        $keyList = explode( '.', $name );
        foreach( $keyList as $keyIndex => $keyValue ) {
           $nextKeyIndex = $keyIndex + 1;
            if( $keyValue == '*' && is_array( $context ) && isset( $keyList[ $nextKeyIndex ] ) ) {
                foreach( $context as $contextIndex => $contextValue ) {
                    if( array_key_exists( $keyList[ $nextKeyIndex ], $contextValue ) && !empty( $contextValue[ $keyList[ $nextKeyIndex ] ] ) ) {
                        $context = $context[ $contextIndex ];
                        continue 2;
                    }
                }
            }
            if( !is_array( $context ) || !array_key_exists( $keyValue, $context ) ) {
                return null;
            }
            $context = $context[ $keyValue ];
        }
        return $context;
    }

    public function initStorage()
    {
        if( is_null( $this->storage ) ) {
            $this->storage = Storage::newInstance( $this );
        }
        return $this->storage;
    }

    public function designCSSClasses()
    {
        $classes = $this->configuration[ 'design' ][ 'name' ] . '-p-form';
        if( $this->configuration[ 'design' ][ 'color' ] != 'default' ) {
            $classes .= ' p-form-' . $this->configuration[ 'design' ][ 'name' ] . '-' . $this->configuration[ 'design' ][ 'color' ];
        }
        if( $this->configuration[ 'rtl' ] ) {
            $classes .= ' ' . $this->configuration[ 'design' ][ 'name' ] . '-ao-form-rtl';
        }
        echo $classes;
    }

    public function attributeList()
    {
        return $this->attributeList;
    }

    public function cloneIndexList( $identifier )
    {
        if( !isset( $this->configuration[ 'attribute_list' ][ $identifier ] ) ) {
            Debug::errorMessage( 'The cloneIndexList method: "' . $identifier . '" attribute is not defined in the config file.', $this->configFile );
            return;
        }
        if( !$this->isCloneableAttribute( $identifier ) ) {
            Debug::errorMessage( 'The "' . $identifier . '" attribute is not cloneable.', $this->configFile );
            return;
        }
        if( !$this->isSubmitted ) {
            return array();
        }
        if( !( isset( $this->data[ 'attribute_list' ][ $identifier ][ 'data' ] ) && is_array( $this->data[ 'attribute_list' ][ $identifier ][ 'data' ] ) ) ) {
            return array();
        }
        return array_keys( $this->data[ 'attribute_list' ][ $identifier ][ 'data' ] );
    }

    public function attributeView( $identifier, $index = false, $settings = array() )
    {
        if( empty( $this->configuration[ 'attribute_list' ] ) ) {
            return;
        }
        if( !isset( $this->configuration[ 'attribute_list' ][ $identifier ] ) ) {
            Debug::errorMessage( 'The attributeView method: "' . $identifier . '" attribute is not defined in the config file.', $this->configFile );
            return;
        }
        $cloneable = $this->isCloneableAttribute( $identifier );
        if( !$cloneable ) {
            if( empty( $this->variableList[ 'attributes_left' ] ) ) {
                Debug::errorMessage( 'There are no attributes left to output', $this->configFile );
                return;
            }
            if( ( $key = array_search( $identifier, $this->variableList[ 'attributes_left' ] ) ) !== false ) {
                unset( $this->variableList[ 'attributes_left' ][ $key ] );
            } else {
                Debug::errorMessage( 'The attributeView method: "' . $identifier . '" attribute is already outputted somewhere on the page.', $this->configFile );
                return;
            }
        }
        if( !$this->variableList[ 'is_form_id_output' ] && ( !$cloneable || $index !== false ) ) {
            $this->formIdValidationAttribute();
            $this->variableList[ 'is_form_id_output' ] = true;
        }
        $clone = array( 'name' => '', 'id' => '' );
        if( $cloneable ) {
            $cloneID = ( $index === false ) ? '{$i}' : intval( $index );
            $clone[ 'name' ] = '[' . $cloneID . ']';
            $clone[ 'id' ] = 'clone' . $cloneID;
            echo '<input type="hidden" name="clone' . $identifier . '[]" value="' . $cloneID . '" />';
        }
        $attribute = array_replace_recursive( $this->configuration[ 'attribute_list' ][ $identifier ], $settings );
        $value = null;
        if( $this->isSubmitted ) {
            if( $cloneable ) {
                $value = ( $index !== false && isset( $this->data[ 'attribute_list' ][ $identifier ][ 'data' ][ $index ] ) ) ? $this->data[ 'attribute_list' ][ $identifier ][ 'data' ][ $index ] : null;
            } else {
                $value = isset( $this->data[ 'attribute_list' ][ $identifier ][ 'data' ] ) ? $this->data[ 'attribute_list' ][ $identifier ][ 'data' ] : null;
            }
        }
        $this->includeTemplate(
            'attributeView/' . $this->configuration[ 'attribute_list' ][ $identifier ][ 'template' ] . '.html.php',
            array(
                'identifier'    => $identifier,
                'attribute'     => $attribute,
                'value'         => $value,
                'isSubmitted'   => $this->isSubmitted,
                'configuration' => $this->configuration,
                'clone'         => $clone
            )
        );
    }

    public function attributeResult( $identifier )
    {
        if( !$this->isSubmitted || !empty( $this->errorList ) ) {
            return;
        }
        if( !isset( $this->configuration[ 'attribute_list' ][ $identifier ] ) ) {
            Debug::errorMessage( 'The attributeResult method: "' . $identifier . '" attribute is not defined in the config file.' );
            return;
        }
        $this->includeTemplate(
            'attributeResult.html.php',
            array(
                'data'      => $this->data[ 'attribute_list' ][ $identifier ],
                'attribute' => $this->configuration[ 'attribute_list' ][ $identifier ]
            )
        );
    }

    public function includeTemplate( $templatePath, $templateVars = array() )
    {
        $templateFullPath = null;
        if( isset( $this->variableList[ 'template_path_list' ][ $templatePath ] ) ) {
            $templateFullPath = $this->variableList[ 'template_path_list' ][ $templatePath ];
        } else {
            if( file_exists( self::getApplicationDirectory() . '/app/views/designOverride/' . $this->configuration[ 'design' ][ 'name' ] . '/' . $templatePath ) ) {
                $templateFullPath = self::getApplicationDirectory() . '/app/views/designOverride/' . $this->configuration[ 'design' ][ 'name' ] . '/' . $templatePath;
            } elseif( file_exists( self::getApplicationDirectory() . '/app/views/' . $templatePath ) ) {
                $templateFullPath = self::getApplicationDirectory() . '/app/views/' . $templatePath;
            } elseif( file_exists( __DIR__ . '/Resources/views/designOverride/' . $this->configuration[ 'design' ][ 'name' ] . '/' . $templatePath ) ) {
                $templateFullPath =  __DIR__ . '/Resources/views/designOverride/' . $this->configuration[ 'design' ][ 'name' ] . '/' . $templatePath;
            } elseif( file_exists( __DIR__ . '/Resources/views/' . $templatePath ) ) {
                $templateFullPath = __DIR__ . '/Resources/views/' . $templatePath;
            }
            if( empty( $templateFullPath ) ) {
                Debug::errorMessage( 'Template "' . $templatePath . '" cannot be found.' );
                return;
            }
            $this->variableList[ 'template_path_list' ][ $templatePath ] = $templateFullPath;
        }
        foreach( $templateVars as $varName => $varValue ) {
            $$varName = $varValue;
        }
        include $templateFullPath;
    }

    public function getTemplateResult( $templatePath, $templateVars = array() )
    {
        ob_start();
        $this->includeTemplate( $templatePath, $templateVars );
        $result = ob_get_contents();
        ob_end_clean();
        return $result;
    }

    public function attributeValue( $identifier )
    {
        if( !$this->isSubmitted || !empty( $this->errorList ) ) {
            return;
        }
        if( !isset( $this->configuration[ 'attribute_list' ][ $identifier ] ) ) {
            Debug::errorMessage( 'In attributeValue method, attribute "' . $identifier . '" is not defined in the config file.' );
            return;
        }
        return isset( $this->data[ 'attribute_list' ][ $identifier ][ 'value' ] ) ? $this->data[ 'attribute_list' ][ $identifier ][ 'value' ] : null;
    }

    private function formIdValidationAttribute()
    {
        echo '<input type="hidden" name="formid" value="' . $this->formID . '">';
    }

    public static function loadStyles()
    {
        $styles = array_merge(
            array(
                'font-awesome'             => 'font-awesome/css/font-awesome.css',
                'bootstrap'                => 'bootstrap/css/bootstrap.css',
                'bootstrap-rtl'            => 'bootstrap-rtl/css/bootstrap-rtl.css',
                'spectrum'                 => 'js/spectrum/spectrum.css',
                'bootstrap-datetimepicker' => 'js/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
                'jquery-ui-core'           => 'js/jquery-ui/css/core.css',
                'jquery-ui-slider'         => 'js/jquery-ui/css/slider.css',
                'jquery-ui-menu'           => 'js/jquery-ui/css/menu.css',
                'jquery-ui-autocomplete'   => 'js/jquery-ui/css/autocomplete.css'
            ),
            self::$staticVariableList[ 'available_style_list' ]
        );
        foreach( $styles as $styleID => $styleSrc ) {
            if( in_array( $styleID, self::$styles ) ) {
                echo '<link rel="stylesheet" href="' . ( ( strpos( $styleSrc, '/' ) !== false && substr( $styleSrc, strpos( $styleSrc, '/' ) + 1, 1 ) == '/' ) ? '' : self::$staticVariableList[ 'asset_directory_uri' ] ) . $styleSrc . '" type="text/css">';
            }
        }
    }

    public static function loadScripts()
    {
        if( empty( self::$scripts ) ) {
            return;
        }
        $scripts = array(
            'jquery'                               => 'js/jquery.js',
            'googleapis-maps'                      => 'https://maps.googleapis.com/maps/api/js?libraries=places&language=en',
            'jquery-ui-core'                       => 'js/jquery-ui/js/core.js',
            'jquery-ui-widget'                     => 'js/jquery-ui/js/widget.js',
            'jquery-ui-mouse'                      => 'js/jquery-ui/js/mouse.js',
            'jquery-ui-position'                   => 'js/jquery-ui/js/position.js',
            'jquery-ui-menu'                       => 'js/jquery-ui/js/menu.js',
            'jquery-ui-autocomplete'               => 'js/jquery-ui/js/autocomplete.js',
            'jquery-ui-slider'                     => 'js/jquery-ui/js/slider.js',
            'jquery-ui-touch-punch'                => 'js/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js',
            'moment-moment'                        => 'js/moment/moment.js',
            'moment-locale'                        => array(),
            'jquery-validation'                    => 'js/jquery-validation/jquery.validate.js',
            'jquery-validation-additional-methods' => 'js/jquery-validation/additional-methods.js',
            'jquery-masked-input'                  => 'js/jquery-masked-input/jquery.maskedinput.js',
            'bootstrap-datetimepicker'             => 'js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js',
            'query-realperson-plugin'              => 'js/jquery-realperson/jquery.plugin.js',
            'query-realperson-realperson'          => 'js/jquery-realperson/jquery.realperson.js',
            'spectrum'                             => 'js/spectrum/spectrum.js',
            'jquery-form-plugin'                   => 'js/jquery-form-plugin/jquery.form.js',
            'forms-plus'                           => 'js/forms-plus/forms-plus.js',
            'forms-plus-value'                     => 'js/forms-plus/forms-plus-value.js',
            'forms-plus-field'                     => 'js/forms-plus/forms-plus-field.js',
            'forms-plus-file'                      => 'js/forms-plus/forms-plus-file.js',
            'forms-plus-spinner'                   => 'js/forms-plus/forms-plus-spinner.js',
            'forms-plus-validation'                => 'js/forms-plus/forms-plus-validation.js',
            'jquery-validation-localization'       => array(),
            'forms-plus-block'                     => 'js/forms-plus/forms-plus-block.js',
            'forms-plus-elements'                  => 'js/forms-plus/forms-plus-elements.js',
            'forms-plus-sub-selection'             => 'js/forms-plus/forms-plus-sub-selection.js',
            'forms-plus-gelements'                 => 'js/forms-plus/forms-plus-gelements.js',
            'forms-plus-slider'                    => 'js/forms-plus/forms-plus-slider.js',
            'forms-plus-autocomplete'              => 'js/forms-plus/forms-plus-autocomplete.js',
            'forms-plus-ajax'                      => 'js/forms-plus/forms-plus-ajax.js',
            'forms-plus-steps'                     => 'js/forms-plus/forms-plus-steps.js',
            'script'                               => 'js/script.js'
        );
        if( !empty( self::$staticVariableList[ 'available_script_list' ] ) ) {
            $scripts = array_replace_recursive( $scripts, self::$staticVariableList[ 'available_script_list' ] );
        }
        foreach( $scripts as $scriptID => $scriptSrc ) {
            if( in_array( $scriptID, self::$scripts ) && !empty( $scriptSrc ) ) {
                if( is_array( $scriptSrc ) ) {
                    foreach( $scriptSrc as $scriptSubSrc ) {
                        echo '<script src="' . self::$staticVariableList[ 'asset_directory_uri' ] . $scriptSubSrc . '" type="text/javascript"></script>';
                    }
                } else {
                    echo '<script src="' . ( ( strpos( $scriptSrc, '/' ) !== false && substr( $scriptSrc, strpos( $scriptSrc, '/' ) + 1, 1 ) == '/' ) ? '' : self::$staticVariableList[ 'asset_directory_uri' ] ) . $scriptSrc . '" type="text/javascript"></script>';
                }
            }
        }
    }

    public static function outputDebug( $output = true, $wrapperHTML = '<div style="background: white;">%content%</div>' )
    {
        if( $output ) {
            echo Debug::getMessageListHTML( $wrapperHTML );
        } else {
            return Debug::getMessageListHTML( $wrapperHTML );
        }
    }

    private function isCloneableAttribute( $attributeID )
    {
        return !empty( $this->configuration[ 'attribute_list' ][ $attributeID ][ 'cloning' ] );
    }

    private function isEmptyString( $value )
    {
        return ( $value !== null && $value !== false && $value !== '' );
    }

    private function getData()
    {
        foreach( $this->attributeList as $attributeID ) {
            $this->getAttributeDataValueName( $attributeID );
        }
    }

    private function getAttributeDataValueName( $attributeID )
    {
        if( $this->isCloneableAttribute( $attributeID ) ) {
            $cloneIndexList = ( isset( $_POST[ 'clone' . $attributeID ] ) && is_array( $_POST[ 'clone' . $attributeID ] ) ) ? array_values( $_POST[ 'clone' . $attributeID ] ) : array();
            // default file structure does not suit for inner handler, so it must be changed
            $postFiles = array();
            if( isset( $_FILES[ $attributeID ] ) ) {
                foreach( $_FILES[ $attributeID ] as $fileOption => $fileData ) {
                    foreach( $fileData as $fileIndex => $fileValue ) {
                        $postFiles[ $fileIndex ][ $fileOption ] = $fileValue;
                    }
                }
            }
            $attributeDataValueList = array( 'data' => array(), 'value' => array(), 'string_value' => array() );
            foreach( $cloneIndexList as $index => $cloneIndex ) {
                $attributeDataValue = $this->getAttributeDataValueByDatatype(
                    $attributeID,
                    isset( $_POST[ $attributeID ][ $cloneIndex ] ) ? $_POST[ $attributeID ][ $cloneIndex ] : null,
                    !empty( $postFiles[ $cloneIndex ] ) ? $postFiles[ $cloneIndex ] : null
                );
                foreach( $attributeDataValue as $varName => $varValue ) {
                    if( $varName == 'files' && !empty( $varValue ) ) {
                        $this->data[ 'file_list' ][ $attributeID ][ $index ] = $varValue;
                    }
                    $attributeDataValueList[ $varName ][ $index ] = $varValue;
                }
            }
            $attributeDataValueList[ 'final_value' ] = implode( '; ', array_filter( $attributeDataValueList[ 'string_value' ], array( $this, 'isEmptyString' ) ) );
            $this->data[ 'attribute_list' ][ $attributeID ] = $attributeDataValueList;
        } else {
            $attributeDataValue = $this->getAttributeDataValueByDatatype(
                $attributeID,
                isset( $_POST[ $attributeID ] ) ? $_POST[ $attributeID ] : null,
                isset( $_FILES[ $attributeID ] ) ? $_FILES[ $attributeID ] : null
            );
            if( !empty( $attributeDataValue[ 'files' ] ) ) {
                $this->data[ 'file_list' ][ $attributeID ] = $attributeDataValue[ 'files' ];
            }
            $attributeDataValue[ 'final_value' ] = $attributeDataValue[ 'string_value' ];
            $this->data[ 'attribute_list' ][ $attributeID ] = $attributeDataValue;
        }
        $this->data[ 'attribute_list' ][ $attributeID ][ 'name' ] = $this->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
    }

    private function getAttributeDataValueByDatatype( $attributeID, $postData, $postFiles )
    {
        $attribute = $this->configuration[ 'attribute_list' ][ $attributeID ];
        $datatype = $attribute[ 'datatype' ];
        $value = null;
        $stringValue = null;
        $files = null;
        switch( $datatype ) {
            case 'selection':
            case 'colorselector':
            case 'imagepicker':
            case 'blockpicker':
                if( is_null( $postData ) || $postData == '' ) {
                    break;
                }
                $datatypeItemListSettings = array(
                    'selection'     => array( 'option_list', array( 'name' ) ),
                    'colorselector' => array( 'color_list', array( 'color' ) ),
                    'imagepicker'   => array( 'image_list', array( 'caption', 'image' ) ),
                    'blockpicker'   => array( 'block_list', array( 'title', 'image' ) )
                );
                $itemListSettings = $datatypeItemListSettings[ $datatype ];
                if( is_array( $postData ) ) {
                    $value = array();
                    foreach( $postData as $dataItem ) {
                        $value[] = $this->getArrayElemenyByFirstValidID( $attribute[ $itemListSettings[ 0 ] ][ $dataItem ], $itemListSettings[ 1 ] );
                    }
                    if( count( $value ) == 0 ) {
                        $value = null;
                    }
                } else {
                    $value = $this->getArrayElemenyByFirstValidID( $attribute[ $itemListSettings[ 0 ] ][ $postData ], $itemListSettings[ 1 ] );
                }
                break;
            case 'radiobutton':
            case 'checkbox':
            case 'switch':
                if( isset( $attribute[ 'checkbox' ] ) && $datatype == 'checkbox' ) {
                    if( isset( $postData ) ) {
                        $value = true;
                        $stringValue = $attribute[ 'checked_text' ];
                    } else {
                        $stringValue = $attribute[ 'unchecked_text' ];
                    }
                } elseif( isset( $postData[ 'check' ] ) ) {
                    if( is_array( $postData[ 'check' ] ) ) {
                        $value = array();
                        foreach( $postData[ 'check' ] as $dataItem ) {
                            if( !empty( $attribute[ $datatype . '_list' ][ $dataItem ][ 'check_input' ] ) ) {
                                if( isset( $postData[ 'input' ][ $dataItem ] ) && $postData[ 'input' ][ $dataItem ] != '' ) {
                                    $value[] = $postData[ 'input' ][ $dataItem ];
                                }
                            } else {
                                $value[] = $attribute[ $datatype . '_list' ][ $dataItem ][ 'name' ];
                            }
                        }
                        if( count( $value ) == 0 ) {
                            $value = null;
                        }
                    } else {
                        if( !empty( $attribute[ $datatype . '_list' ][ $postData[ 'check' ] ][ 'check_input' ] ) ) {
                            if( isset( $postData[ 'input' ][ $postData[ 'check' ] ] ) && $postData[ 'input' ][ $postData[ 'check' ] ] != '' ) {
                                $value = $postData[ 'input' ][ $postData[ 'check' ] ];
                            }
                        } else {
                            $value = $attribute[ $datatype . '_list' ][ $postData[ 'check' ] ][ 'name' ];
                        }
                    }
                }
                break;
            case 'file':
            case 'imageupload':
                if( is_null( $postFiles ) ) {
                    break;
                }
                if( is_array( $postFiles[ 'name' ] ) ) {
                    $fileOptionNameList = array_keys( $postFiles );
                    foreach( $postFiles[ 'name' ] as $index => $fileName ) {
                        if( $postFiles[ 'error' ][ $index ] != UPLOAD_ERR_OK ) {
                            continue;
                        }
                        if( is_null( $value ) ) {
                            $value = array();
                        }
                        $value[] = $fileName;
                        $fileDataItem = array();
                        foreach( $fileOptionNameList as $fileOptionName ) {
                            $fileDataItem[ $fileOptionName ] = $postFiles[ $fileOptionName ][ $index ];
                        }
                        if( is_null( $files ) ) {
                            $files = array();
                        }
                        $files[] = $fileDataItem;
                    }
                } else {
                    if( $postFiles[ 'error' ] != UPLOAD_ERR_OK ) {
                        break;
                    }
                    $value = $postFiles[ 'name' ];
                    $files = array( $postFiles );
                }
                break;
            case 'inputgroup':
                if( is_null( $postData ) ) {
                    break;
                }
                if( !is_array( $postData ) ) {
                    break;
                }
                $filteredData = array_filter( $postData, array( $this, 'isEmptyString' ) );
                if( count( $filteredData ) == 0 ) {
                    break;
                }
                $value = implode( $attribute[ 'delimiter' ], $filteredData );
                break;
            case 'data':
                if( is_null( $postData ) ) {
                    break;
                }
                if( !is_array( $postData ) && strval( $postData ) == '' ) {
                    break;
                }
                $value = $postData;
                break;
            case 'captcha':
                $stringValue = '';
            default:
                if( is_null( $postData ) ) {
                    break;
                }
                if( is_array( $postData ) ) {
                    if( count( array_filter( $postData, array( $this, 'isEmptyString' ) ) ) == 0 ) {
                        break;
                    }
                } else {
                    if( strval( $postData ) == '' ) {
                        break;
                    }
                }
                $value = $postData;
        }
        if( !empty( $attribute[ 'value_handler' ] ) ) {
            call_user_func_array( $attribute[ 'value_handler' ], array( &$value, &$stringValue, $attributeID, $postData, $postFiles, $this ) );
        }
        return array(
            'data'         => $postData,
            'value'        => $value,
            'string_value' => strval( !is_null( $stringValue ) ? $stringValue : ( is_array( $value ) ? implode( ', ', $value ) : $value ) ),
            'files'        => $files
        );
    }

    private function getArrayElemenyByFirstValidID( $array, $ids )
    {
        foreach( $ids as $id ) {
            if( isset( $array[ $id ] ) ) {
                return $array[ $id ];
            }
        }
        return null;
    }

    private function validateData()
    {
        foreach( $this->attributeList as $attributeID ) {
            if( $this->isCloneableAttribute( $attributeID ) ) {
                foreach( $this->data[ 'attribute_list' ][ $attributeID ][ 'value' ] as $index => $value ) {
                    $this->getRulePreValidationData(
                        $attributeID,
                        $value
                    );
                }
            } else {
                $this->getRulePreValidationData(
                    $attributeID,
                    $this->data[ 'attribute_list' ][ $attributeID ][ 'value' ]
                );
            }
        }
        foreach( $this->attributeList as $attributeID ) {
            if( $this->isCloneableAttribute( $attributeID ) ) {
                $cloneCount = count( $this->data[ 'attribute_list' ][ $attributeID ][ 'value' ] );
                $cloneMin = isset( $this->configuration[ 'attribute_list' ][ $attributeID ][ 'cloning' ][ 'min' ] ) ? intval( $this->configuration[ 'attribute_list' ][ $attributeID ][ 'cloning' ][ 'min' ] ) : 0;
                $cloneMax = isset( $this->configuration[ 'attribute_list' ][ $attributeID ][ 'cloning' ][ 'max' ] ) ? intval( $this->configuration[ 'attribute_list' ][ $attributeID ][ 'cloning' ][ 'max' ] ) : null;
                if( !( $cloneCount >= $cloneMin && ( !isset( $cloneMax ) || $cloneCount <= $cloneMax ) ) ) {
                    $this->errorList[ $attributeID ][ 0 ][ 'cloning' ] = $this->getRuleErrorMessage( $this->configuration[ 'attribute_list' ][ $attributeID ], 'cloning', 'Number of instances (min: ' . ( isset( $cloneMin ) ? $cloneMin : '0' ) . ( isset( $cloneMax ) ? ( ', max: ' .  $cloneMax )  : '' ) . ').' );
                    continue;
                }
                foreach( $this->data[ 'attribute_list' ][ $attributeID ][ 'value' ] as $index => $value ) {
                    $this->validateAttributeValue(
                        $attributeID,
                        $index,
                        $value,
                        isset( $this->data[ 'file_list' ][ $attributeID ][ $index ] ) ? $this->data[ 'file_list' ][ $attributeID ][ $index ] : null,
                        true
                    );
                }
            } else {
                $this->validateAttributeValue(
                    $attributeID,
                    0,
                    $this->data[ 'attribute_list' ][ $attributeID ][ 'value' ],
                    isset( $this->data[ 'file_list' ][ $attributeID ] ) ? $this->data[ 'file_list' ][ $attributeID ] : null,
                    false
                );
            }
        }
    }

    private function getRulePreValidationData( $attributeID, $value )
    {
        $attribute = $this->configuration[ 'attribute_list' ][ $attributeID ];
        if( empty( $attribute[ 'validation' ][ 'rule_list' ] ) ) {
            return;
        }
        foreach( $attribute[ 'validation' ][ 'rule_list' ] as $ruleName => $ruleValue ) {
            switch( $ruleName ) {
                case 'grouprequire':
                    $groupRequireValue = explode( ':', $ruleValue );
                    if( empty( $groupRequireValue[ 0 ] ) ) {
                        break;
                    }
                    $groupRequireName = $groupRequireValue[ 0 ];
                    $this->variableList[ 'validation_rule_data' ][ 'grouprequire' ][ 'attribute_list' ][ $attributeID ] = $groupRequireName;
                    if( !isset( $this->variableList[ 'validation_rule_data' ][ 'grouprequire' ][ 'group_list' ][ $groupRequireName ] ) ) {
                        $this->variableList[ 'validation_rule_data' ][ 'grouprequire' ][ 'group_list' ][ $groupRequireName ][ 'count' ] = 0;
                        $this->variableList[ 'validation_rule_data' ][ 'grouprequire' ][ 'group_list' ][ $groupRequireName ][ 'min' ] = 1;
                        if( isset( $groupRequireValue[ 1 ] ) && ( $groupRequireMin = intval( $groupRequireValue[ 1 ] ) ) >= 0 ) {
                            $this->variableList[ 'validation_rule_data' ][ 'grouprequire' ][ 'group_list' ][ $groupRequireName ][ 'min' ] = $groupRequireMin;
                        }
                        if( isset( $groupRequireValue[ 2 ] ) && ( $groupRequireMax = intval( $groupRequireValue[ 2 ] ) ) > 0 ) {
                            $this->variableList[ 'validation_rule_data' ][ 'grouprequire' ][ 'group_list' ][ $groupRequireName ][ 'max' ] = $groupRequireMax;
                        }
                    }
                    if( !is_null( $value ) ) {
                        $this->variableList[ 'validation_rule_data' ][ 'grouprequire' ][ 'group_list' ][ $groupRequireName ][ 'count' ] += is_array( $value ) ? count( $value ) : 1;
                    }
                    break;
            }
        }
    }

    private function validateAttributeValue( $attributeID, $index, $value, $files, $cloneable )
    {
        $attribute = $this->configuration[ 'attribute_list' ][ $attributeID ];
        $validationRuleList = !empty( $attribute[ 'validation' ][ 'rule_list' ] ) ? $attribute[ 'validation' ][ 'rule_list' ] : array();
        if( ( isset( $attribute[ 'validation' ][ 'ignore' ] ) && $attribute[ 'validation' ][ 'ignore' ] ) || ( isset( $attribute[ 'validation' ][ 'ignore_hidden' ] ) && $attribute[ 'validation' ][ 'ignore_hidden' ] ) ) {
            // if validation is ignored, there is no need to perform validation
            return;
        }
        // the required rules has the hiest priority
        if( is_null( $value ) && ( ( isset( $attribute[ 'required' ] ) && $attribute[ 'required' ] ) || ( isset( $validationRuleList[ 'required' ] ) && $validationRuleList[ 'required' ] ) ) ) {
            $this->errorList[ $attributeID ][ $index ][ 'required' ] = $this->getRuleErrorMessage( $attribute, 'required', 'This field is required.' );
            return;
        }
        // grouprequire rules
        if( isset( $this->variableList[ 'validation_rule_data' ][ 'grouprequire' ][ 'attribute_list' ][ $attributeID ] ) ) {
            $groupRequireData = $this->variableList[ 'validation_rule_data' ][ 'grouprequire' ][ 'group_list' ][ $this->variableList[ 'validation_rule_data' ][ 'grouprequire' ][ 'attribute_list' ][ $attributeID ] ];
            if( !( $groupRequireData[ 'count' ] >= $groupRequireData[ 'min' ] && ( !isset( $groupRequireData[ 'max' ] ) || $groupRequireData[ 'count' ] <= $groupRequireData[ 'max' ] ) ) ) {
                $this->errorList[ $attributeID ][ $index ][ 'rule_grouprequire' ] = $this->getRuleErrorMessage( $attribute, 'grouprequire', 'Please fill some of these fields (min: ' . ( isset( $groupRequireData[ 'min' ] ) ? $groupRequireData[ 'min' ] : '1' ) . ( isset( $groupRequireData[ 'max' ] ) ? ( ', max: ' .  $groupRequireData[ 'max' ] )  : '' ) . ').' );
                return;
            }
        }
        if( is_null( $value ) ) {
            // if the value is empty and it hasn't triggered any "required" rules then other validation can be skipped
            return;
        }
        switch( $attribute[ 'datatype' ] ) {
            case 'email':
                if( preg_match( "/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/", $value ) === 0 ) {
                    $this->errorList[ $attributeID ][ $index ][ 'datatype_email' ] = $this->getRuleErrorMessage( $attribute, 'email', 'Please enter a valid email address' );
                }
                break;
            case 'url':
                if( filter_var( $value, FILTER_VALIDATE_URL ) === false ) {
                    $this->errorList[ $attributeID ][ $index ][ 'datatype_url' ] = $this->getRuleErrorMessage( $attribute, 'url', 'Please enter a valid URL' );
                }
                break;
            case 'file':
            case 'imageupload':
                if( !empty( $attribute[ 'accept' ] ) ) {
                    $validationRuleList[ 'accept' ] = $attribute[ 'accept' ];
                }
                break;
            case 'captcha':
                $validationRuleList[ 'captcha' ] = true;
                break;
        }
        // native HTML5 input attributes for validation should be processed by PHP as well
        if( isset( $attribute[ 'min' ] ) ) {
            $validationRuleList[ 'min' ] = $attribute[ 'min' ];
        }
        if( isset( $attribute[ 'max' ] ) ) {
            $validationRuleList[ 'max' ] = $attribute[ 'max' ];
        }
        // validation by validation rule list in attribute
        foreach( $validationRuleList as $ruleName => $ruleValue ) {
            switch( $ruleName ) {
                case 'dateiso':
                    if( preg_match( "/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/", $value ) === 0 ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, 'Please enter a valid date ( ISO ).' );
                    }
                    break;
                case 'number':
                    if( preg_match( "/^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/", $value ) === 0 ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, 'Please enter a valid number.' );
                    }
                    break;
                case 'digits':
                    if( preg_match( "/^\d+$/", $value ) === 0 ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, 'Please enter only digits.' );
                    }
                    break;
                case 'creditcard':
                    if( !Validation::checkCreditCard( $value ) ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, 'Please enter a valid credit card number.' );
                    }
                    break;
                case 'minlength':
                    if( mb_strlen( $value, 'UTF-8' ) < $ruleValue ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, "Please enter at least $ruleValue characters." );
                    }
                    break;
                case 'maxlength':
                    if( mb_strlen( $value, 'UTF-8' ) > $ruleValue ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, "Please enter no more than $ruleValue characters." );
                    }
                    break;
                case 'rangelength':
                    if( !is_array( $ruleValue ) || count( $ruleValue ) != 2 ) {
                        break;
                    }
                    if( mb_strlen( $value, 'UTF-8' ) < $ruleValue[ 0 ] || mb_strlen( $value, 'UTF-8' ) > $ruleValue[ 1 ] ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, 'Please enter a value between ' . $ruleValue[ 0 ] . ' and ' . $ruleValue[ 1 ] . ' characters long.' );
                    }
                    break;
                case 'min':
                    if( preg_match( "/^[0-9\.\-]+$/", $value ) === 0 || floatval( $value ) < $ruleValue ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, "Please enter a value greater than or equal to $ruleValue." );
                    }
                    break;
                case 'max':
                    if( preg_match( "/^[0-9\.\-]+$/", $value ) === 0 || floatval( $value ) > $ruleValue ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, "Please enter a value less than or equal to $ruleValue." );
                    }
                    break;
                case 'range':
                    if( !is_array( $ruleValue ) || count( $ruleValue ) != 2 ) {
                        break;
                    }
                    if( preg_match( "/^[0-9\.\-]+$/", $value ) === 0 || floatval( $value ) < $ruleValue[ 0 ] || floatval( $value ) > $ruleValue[ 1 ] ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, 'Please enter a value between ' . $ruleValue[ 0 ] . ' and ' . $ruleValue[ 1 ] . '.' );
                    }
                    break;
                case 'equalto':
                    $equalToAttributeID = ltrim( $ruleValue, '#' );
                    if( !isset( $this->data[ 'attribute_list' ][ $equalToAttributeID ] ) || $this->data[ 'attribute_list' ][ $equalToAttributeID ][ 'value' ] != $value ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, 'Please enter the same value again.' );
                    }
                    break;
                case 'dotmail':
                    if( preg_match( "/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])+)+$/", $value ) === 0 ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, 'Please enter a valid email address.' );
                    }
                    break;
                case 'filesize':
                    $fileSizeUnitSymbol = strtolower( substr( $ruleValue, -2, 1 ) );
                    $fileSizeConversionByUnits = array( 'k' => 1024, 'm' => 1048576, 'g' => 1073741824 );
                    $fileSizeInBytes = isset( $fileSizeConversionByUnits[ $fileSizeUnitSymbol ] ) ? ( floatval( substr( $ruleValue, 0, -2 ) ) * $fileSizeConversionByUnits[ $fileSizeUnitSymbol ] ) : floatval( $ruleValue );
                    foreach( $files as $file ) {
                        if( $file[ 'size' ] > $fileSizeInBytes ) {
                            $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, "File size should be less than $ruleValue." );
                            break;
                        }
                    }
                    break;
                case 'accept':
                    $acceptTypes = explode( ',', $ruleValue );
                    $acceptTypesRegExp = array();
                    foreach( $acceptTypes as $acceptType ) {
                        $acceptTypesRegExp[] = str_replace( '\*', '.*', preg_quote( $acceptType, '/' ) );
                    }
                    $acceptRegExpPattern = '/^(' . implode( '|', $acceptTypesRegExp ) . ')$/';
                    foreach( $files as $file ) {
                        if( preg_match( $acceptRegExpPattern, $file[ 'type' ] ) === 0 ) {
                            $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, 'Please enter a value with a valid mimetype.' );
                            break;
                        }
                    }
                    break;
                case 'captcha':
                    $captchaHash = $cloneable ? ( isset( $_POST[ 'hash' . $attributeID ][ $index ] ) ? $_POST[ 'hash' . $attributeID ][ $index ] : null ) : ( isset( $_POST[ 'hash' . $attributeID ] ) ? $_POST[ 'hash' . $attributeID ] : null );
                    if( !( !is_null( $captchaHash ) && $captchaHash == Validation::getCaptchaHash( $value ) ) ) {
                        $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, 'Please enter correct code.' );
                    }
                    break;
            }
            if( substr( $ruleName, 0, 7 ) === 'backend' ) {
                $last = false;
                $callback = ( is_array( $ruleValue ) && isset( $ruleValue[ 0 ] ) ) ? $ruleValue[ 0 ] : $ruleValue;
                $parameterList = ( is_array( $ruleValue ) && isset( $ruleValue[ 1 ] ) ) ? $ruleValue[ 1 ] : null;
                $result = call_user_func_array( $callback, array( $value, $files, $attributeID, $this, $parameterList, &$last ) );
                if( $result ) {
                    $this->errorList[ $attributeID ][ $index ][ 'rule_' . $ruleName ] = $this->getRuleErrorMessage( $attribute, $ruleName, is_string( $result ) ? $result : 'Internal validation is not passed' );
                    if( $last ) {
                        break;
                    }
                }
            }
        }
    }

    private function getRuleErrorMessage( $attribute, $ruleName, $message )
    {
        return isset( $attribute[ 'validation' ][ 'message_list' ][ $ruleName ] ) ? $attribute[ 'validation' ][ 'message_list' ][ $ruleName ] : $message;
    }

    public function showBlockHeadingStart( $identifier, $multiple = false, $selected = false, $cssClass = '' )
    {
        if( preg_match( '/[^a-z0-9_\-]/', $identifier ) ) {
            Debug::warningMessage( 'The show block identifier "' . $identifier . '" is wrong, a show block identifier can only contain small latin letters (a-z), numbers (0-9), dashes and underscores.', $this->configFile );
            return;
        }
        if( isset( self::$staticVariableList[ 'used_showblock_identifier_list' ][ $identifier ] ) ) {
            self::$staticVariableList[ 'used_showblock_identifier_list' ][ $identifier ]++;
        } else {
            self::$staticVariableList[ 'used_showblock_identifier_list' ][ $identifier ] = 0;
        }
        $value = self::$staticVariableList[ 'used_showblock_identifier_list' ][ $identifier ];
        $inputName = 'showblock' . $identifier;
        $inputId = 'showblock' . $identifier . $value;
        $inputType = $multiple ? 'checkbox' : 'radio';
        if( $this->isSubmitted ) {
            $selected = ( isset( $_POST[ $inputName ] ) ) ? ( is_array( $_POST[ $inputName ] ) ? in_array( $value, $_POST[ $inputName ] ) : ( $_POST[ $inputName ] == $value ) ) : false;
        }
        echo '<input type="' . $inputType . '" id="' . $inputId . '" name="' . $inputName . ( $multiple ? '[]' : '' ) . '" value="' . $value . '" class="p-show-block p-check-next"' . ( $selected ? ' checked ' : '' ) . '/>';
        echo '<label for="' . $inputId . '" name="' . $inputName . '" class="p-show-block-heading p-check-container clearfix ' . $cssClass . '">';
    }

    public function showBlockHeadingEnd()
    {
        echo '</label>';
    }

    public static function getApplicationDirectory()
    {
        if( isset( self::$staticVariableList[ 'application_root_directory' ] ) ) {
            return self::$staticVariableList[ 'application_root_directory' ];
        } else {
            self::$staticVariableList[ 'application_root_directory' ] = realpath( __DIR__ . '/../../../' );
            return self::$staticVariableList[ 'application_root_directory' ];
        }
    }
}
