<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/ajax-form-comment.yml' );
    if( $form->isSubmitted ) {
        header( 'Content-Type: application/json' );
        if( $form->isValid ) {
            $data = array(
                'block'   => 'successContentBlock',
                'content' => '<div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>'
            );
            $data[ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        } else {
            $content = '';
            foreach( $form->errorList as $attributeID => $itemList ) {
                $name = $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
                $content .= '<div class="alert alert-error"><strong><i class="fa fa-times"></i> ' . $name . ':</strong> ';
                if( count( $itemList ) == 1 ) {
                    foreach( $itemList as $errorList ) $content .=  implode( ' ', $errorList );
                } else {
                    $content .= '<ul>';
                    foreach( $itemList as $errorList ) $content .= '<li>' . implode( ' ', $errorList ) . '</li>';
                    $content .= '</ul>';
                }
                $content .= '</div>';
            }
            $data = array( 'errorData' => array(
                'block'   => 'errorContentBlock',
                'content' => $content
            ));
            $data[ 'errorData' ][ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        }
        echo json_encode( $data );
        exit;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true" data-js-ajax-form="" data-js-ajax-before-hide-block="successBlockName;failBlockName" data-js-ajax-before-show-block="loadingBlockName" data-js-ajax-success-show-block="successBlockName" data-js-ajax-success-hide-block="formBlockName" data-js-ajax-fail-show-block="failBlockName" data-js-ajax-always-hide-block="loadingBlockName">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">Comment&nbsp;&nbsp;<i class="fa fa-comment-o"></i></span>
                    </div>
                    <div data-js-block="successBlockName" class="collapse">
                        <h4>Form was sent successfully!</h4>
                        <div data-js-block="successContentBlock" class="collapse"></div>
                        <div class="text-right">
                            <a href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="btn">reload</a>
                        </div>
                    </div>
                    <div data-js-block="failBlockName" class="collapse">
                        <h4>Failed to send form!</h4>
                        <div data-js-block="errorContentBlock" class="collapse"></div>
                    </div>
                    <div data-js-block="formBlockName">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'name' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'email' ); ?>
                            </div>
                        </div>
                        <?php $form->attributeView( 'comment' ); ?>
                        <?php $form->attributeView( 'captcha' ); ?>
                        <div class="clearfix"></div>
                        <?php $form->attributeView( 'notify' ); ?>
                        <div data-js-block="loadingBlockName" class="progress collapse">
                            <div class="progress-bar progress-bar-fp progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                        <div class="text-right">
                            <button class="btn p-ajax-disabled" type="submit" name="confirm"><i class="fa fa-share-square-o"></i>&nbsp;post comment</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>