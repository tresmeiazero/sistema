<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form1 = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/tabs-1.yml' );
    $form2 = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/tabs-2.yml' );
    $form3 = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/tabs-3.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="<?php $form1->designCSSClasses(); ?>">
                <div class="p-form p-shadowed p-form-sm">
                    <input type="radio" id="tab-1" name="activeTab" class="p-tab-sel"<?php echo ( $form1->isSubmitted || ( !$form1->isSubmitted && !$form2->isSubmitted && !$form3->isSubmitted ) ) ? ' checked' : ''; ?>/>
                    <input type="radio" id="tab-2" name="activeTab" class="p-tab-sel"<?php echo $form2->isSubmitted ? ' checked' : ''; ?>/>
                    <input type="radio" id="tab-3" name="activeTab" class="p-tab-sel"<?php echo $form3->isSubmitted ? ' checked' : ''; ?>/>
                    <ul class="nav nav-tabs">
                        <li><label for="tab-1">Tab 1</label></li>
                        <li><label for="tab-2">Tab 2</label></li>
                        <li><label for="tab-3">Tab 3</label></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane">
                            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>">
                                <?php if( $form1->isValid ) { ?>
                                    <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                                <?php } elseif( $form1->isSubmitted ) { ?>
                                    <?php foreach( $form1->errorList as $attributeID => $itemList ) { ?>
                                        <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form1->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                            <?php if( count( $itemList ) == 1 ) { ?>
                                                <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                            <?php } else { ?>
                                                <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <?php if( !$form1->isValid ) { ?>
                                    <div class="p-subtitle text-left">
                                        <span class="p-title-side">Tab 1</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php $form1->attributeView( 'text' ); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php $form1->attributeView( 'password' ); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php $form1->attributeView( 'email' ); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php $form1->attributeView( 'url' ); ?>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn" type="submit">submit</button>
                                        <button class="btn" type="reset">reset</button>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                        <div class="tab-pane">
                            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>">
                                <?php if( $form2->isValid ) { ?>
                                    <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                                <?php } elseif( $form2->isSubmitted ) { ?>
                                    <?php foreach( $form2->errorList as $attributeID => $itemList ) { ?>
                                        <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form2->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                            <?php if( count( $itemList ) == 1 ) { ?>
                                                <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                            <?php } else { ?>
                                                <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <?php if( !$form2->isValid ) { ?>
                                    <div class="p-subtitle text-left">
                                        <span class="p-title-side">Tab 2</span>
                                    </div>
                                    <?php $form2->attributeView( 'search' ); ?>
                                    <?php $form2->attributeView( 'textarea' ); ?>
                                    <div class="text-right">
                                        <button class="btn" type="submit">submit</button>
                                        <button class="btn" type="reset">reset</button>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                        <div class="tab-pane">
                            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>">
                                <?php if( $form3->isValid ) { ?>
                                    <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                                <?php } elseif( $form3->isSubmitted ) { ?>
                                    <?php foreach( $form3->errorList as $attributeID => $itemList ) { ?>
                                        <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form3->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                            <?php if( count( $itemList ) == 1 ) { ?>
                                                <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                            <?php } else { ?>
                                                <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <?php if( !$form3->isValid ) { ?>
                                    <div class="p-subtitle text-left">
                                        <span class="p-title-side">Tab 3</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php $form3->attributeView( 'email_with_icon' ); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php $form3->attributeView( 'price' ); ?>
                                        </div>
                                    </div>
                                    <?php $form3->attributeView( 'email_with_addon' ); ?>
                                    <div class="text-right">
                                        <button class="btn" type="submit">submit</button>
                                        <button class="btn" type="reset">reset</button>
                                    </div>
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>