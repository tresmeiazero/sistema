<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/ajax-form-order-service-steps.yml' );
    if( $form->isSubmitted ) {
        header( 'Content-Type: application/json' );
        if( $form->isValid ) {
            $data = array(
                'block'   => 'successContentBlock',
                'content' => '<div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>'
            );
            $data[ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
            ob_start();
            ?>
            <div class="p-subtitle text-left">
                <span class="p-title-side">Order details</span>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'service' ); ?>
                </div>
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'budget' ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'date_from' ); ?>
                </div>
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'date_to' ); ?>
                </div>
            </div>
            <div class="p-subtitle text-left">
                <span class="p-title-side">Your details</span>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'name' ); ?>
                </div>
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'company' ); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'email' ); ?>
                </div>
                <div class="col-sm-6">
                    <?php $form->attributeResult( 'contact_phone' ); ?>
                </div>
            </div>
            <?php $form->attributeResult( 'message' ); ?>
            <?php
            $content = ob_get_contents();
            ob_end_clean();
            $data[ 'content' ] .= $content;
        } else {
            $content = '';
            $step = false;
            foreach( $form->errorList as $attributeID => $itemList ) {
                $name = $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ];
                $content .= '<div class="alert alert-error"><strong><i class="fa fa-times"></i> ' . $name . ':</strong> ';
                if( count( $itemList ) == 1 ) {
                    foreach( $itemList as $errorList ) $content .=  implode( ' ', $errorList );
                } else {
                    $content .= '<ul>';
                    foreach( $itemList as $errorList ) $content .= '<li>' . implode( ' ', $errorList ) . '</li>';
                    $content .= '</ul>';
                }
                $content .= '</div>';
                if( !empty( $form->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) && ( $step === false || $step > $form->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ] ) ) {
                    $step = $form->configuration[ 'attribute_list' ][ $attributeID ][ 'form_step' ];
                }
            }
            $data = array( 'errorData' => array(
                'block'   => 'errorContentBlock',
                'content' => $content
            ));
            if( $step !== false ) {
                $data[ 'errorData' ][ 'step' ] = $step;
            }
            $data[ 'errorData' ][ 'content' ] .= FormsPlusFramework::outputDebug( false, '<div class="alert alert-error">%content%</div>' );
        }
        echo json_encode( $data );
        exit;
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true" data-js-ajax-form="" data-js-ajax-before-hide-block="successBlockName;failBlockName" data-js-ajax-before-show-block="loadingBlockName" data-js-ajax-success-show-block="successBlockName" data-js-ajax-success-hide-block="formBlockName" data-js-ajax-fail-show-block="failBlockName" data-js-ajax-always-hide-block="loadingBlockName">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-form-steps-wrap">
                        <ul class="p-form-steps" data-js-stepper="bookingSteps">
                            <li class="active" data-js-step="detailsBlock">
                                <span class="p-step">
                                    <span class="p-step-text">Details</span>
                                </span>
                            </li>
                            <li data-js-step="customerBlock">
                                <span class="p-step">
                                    <span class="p-step-text">Customer</span>
                                </span>
                            </li>
                            <li data-js-step="invoiceBlock" data-js-disable-steps="true">
                                <span class="p-step">
                                    <span class="p-step-text">Invoice</span>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div data-js-block="errorContentBlock" class="collapse"></div>
                    <div data-js-block="detailsBlock" data-js-validation-block="">
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Order details</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                 <?php $form->attributeView( 'service' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'budget' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'date_from' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'date_to' ); ?>
                            </div>
                        </div>
                        <div class="text-right">
                            <a class="btn" data-js-show-step="bookingSteps:2"><i class="fa fa-check-square-o"></i>&nbsp;confirm</a>
                            <a class="btn" href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>"><i class="fa fa-ban"></i>&nbsp;cancel</a>
                        </div>
                    </div>
                    <div data-js-block="customerBlock" class="collapse">
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Your details</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'name' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'company' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'email' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'contact_phone' ); ?>
                            </div>
                        </div>
                        <?php $form->attributeView( 'message' ); ?>
                        <div data-js-block="loadingBlockName" class="progress collapse">
                            <div class="progress-bar progress-bar-fp progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                        </div>
                        <div class="text-right">
                            <a class="btn" href="#" data-js-show-step="bookingSteps:1"><i class="fa fa-arrow-left"></i>&nbsp;back</a>
                            <button class="btn p-ajax-disabled" type="submit" name="confirm" data-js-show-step="bookingSteps:3"><i class="fa fa-check-square-o"></i>&nbsp;confirm</button>
                        </div>
                    </div>
                    <div data-js-block="invoiceBlock" class="collapse">
                        <h4>Ajax success!</h4>
                        <div data-js-block="successContentBlock" class="collapse"></div>
                        <div class="text-right">
                            <a href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="btn">reload</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>