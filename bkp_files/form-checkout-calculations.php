<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-checkout-calculations.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">Checkout&nbsp;&nbsp;<i class="fa fa-shopping-cart"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <div class="collapse" data-js-watch-value="" data-js-just-calculations="" data-js-group-values="cartItems:#itemsPrice*" data-js-empty-value-class="show">
                            <h3>Cart is empty.</h3>
                            <div class="text-right">
                                <a href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="btn">reload</a>
                            </div>
                        </div>
                        <div data-js-watch-value="" data-js-just-calculations="" data-js-group-values="cartItems:#itemsPrice*" data-js-empty-value-class="hide">
                            <div class="p-subtitle text-left">
                                <span class="p-title-side">billing details</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'first_name' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'last_name' ); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'email' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'contact_phone' ); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'company' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'company_id' ); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'billing_address' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'postal_code' ); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'city' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'country' ); ?>
                                </div>
                            </div>
                            <div class="p-subtitle text-left">
                                <span class="p-title-side">payment type</span>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <?php $form->attributeView( 'select_card' ); ?>
                                </div>
                                <div class="col-sm-7">
                                    <?php $form->attributeView( 'credit_card_holder_name' ); ?>
                                    <?php $form->attributeView( 'credit_card_number' ); ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php $form->attributeView( 'validation_date' ); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php $form->attributeView( 'cvv2' ); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="p-subtitle text-left">
                                <span class="p-title-side">order info</span>
                            </div>
                            <table class="table table-striped p-table">
                                <thead>
                                    <tr>
                                        <td>Product name</td>
                                        <td class="text-center">Quantity</td>
                                        <td class="text-center">Price</td>
                                        <td class="p-price-column hidden-xs">Total price</td>
                                        <td class="p-action-column">&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="p-block">
                                        <td>Product name 1</td>
                                        <td class="text-center">2</td>
                                        <td class="p-price-column">100$</td>
                                        <td class="p-price-column hidden-xs">200$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[0][id]" value="1" />
                                            <input type="hidden" name="product[0][quantity]" value="2" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:100}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <tr class="p-block">
                                        <td>Product name 2</td>
                                        <td class="text-center">1</td>
                                        <td class="p-price-column">500$</td>
                                        <td class="p-price-column hidden-xs">500$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[1][id]" value="2" />
                                            <input type="hidden" name="product[1][quantity]" value="1" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:500}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <tr class="p-block">
                                        <td>Product name 3</td>
                                        <td class="text-center">3</td>
                                        <td class="p-price-column">120$</td>
                                        <td class="p-price-column hidden-xs">360$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[2][id]" value="3" />
                                            <input type="hidden" name="product[2][quantity]" value="3" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:120}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                    <tr class="p-block">
                                        <td>Product name 4</td>
                                        <td class="text-center">2</td>
                                        <td class="p-price-column">180$</td>
                                        <td class="p-price-column hidden-xs">360$</td>
                                        <td class="p-action-column">
                                            <input type="hidden" name="product[3][id]" value="4" />
                                            <input type="hidden" name="product[3][quantity]" value="2" data-js-value-group="cartItems" data-js-value-format="{#quantity*}" data-js-get-value-extra="{#price:180}{#itemsPrice:price*quantity}" />
                                            <a href="#" class="p-action-link" data-js-remove-block=""><i class="fa fa-times"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr />
                            <div class="p-total-block">
                                <div class="p-action-column pull-right hidden-xs"></div>
                                <div class="pull-right">
                                    <div class="clearfix"></div>
                                    <div class="pull-right">
                                        <div class="pull-left text-right">Product price:</div>
                                        <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-value-group="cartProductPrice" data-js-group-values="cartItems:#itemsPrice*" data-js-value-format="{#2#itemsPrice*}{currency:'$'}"></div>
                                        <input type="hidden" name="price[product]" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="cartItems:#itemsPrice*" data-js-value-format="{#2#itemsPrice*}{currency:'$'}"/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="pull-right">
                                        <div class="pull-left text-right">Tax 20%:</div>
                                        <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-value-group="cartTax" data-js-group-values="cartProductPrice:#itemsPrice,currency" data-js-value-format="{#2#tax*:itemsPrice*0.2}{currency}"></div>
                                        <input type="hidden" name="price[tax]" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="cartProductPrice:#itemsPrice,currency" data-js-value-format="{#2#tax*:itemsPrice*0.2}{currency}"/>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="pull-right">
                                        <div class="pull-left text-right">Delivery price:</div>
                                        <div class="p-price-column">10$</div>
                                        <input type="hidden" name="price[delivery]" value="10$" />
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="pull-right p-colored-text">
                                        <div class="pull-left text-right"><i class="fa fa-shopping-cart"></i> Subtotal:</div>
                                        <div class="p-price-column" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="cartTax:#tax*,currency;cartProductPrice:#itemsPrice*" data-js-value-format="{#2#*:itemsPrice+tax+10}{currency}"></div>
                                        <input type="hidden" name="price[subtotal]" data-js-watch-value="" data-js-empty-value="-" data-js-group-values="cartTax:#tax*,currency;cartProductPrice:#itemsPrice*" data-js-value-format="{#2#*:itemsPrice+tax+10}{currency}"/>
                                    </div>
                                </div>
                            </div>
                            <hr class="p-flat" />
                            <?php $form->attributeView( 'additional_message' ); ?>
                            <?php $form->attributeView( 'terms_of_service' ); ?>
                            <div class="text-right">
                                <button class="btn" type="submit" name="confirm"><i class="fa fa-check-square-o"></i>&nbsp;confirm</button>
                                <a class="btn" href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>"><i class="fa fa-ban"></i>&nbsp;cancel</a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>