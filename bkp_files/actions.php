<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/actions.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">actions&nbsp;&nbsp;<i class="fa fa-hand-pointer-o"></i></span>
                    </div>
                    <div class="p-subtitle text-left">
                        <span class="p-title-side">Add block</span>
                    </div>
                    <script data-js-template="template_add_block_default" type="text/x-tmpl">
                        <div class="p-block" data-js-block-group="addBlockDefault">
                            <?php $form->attributeView( 'add_block_default', false, array( 'label' => null ) ); ?>
                            <span class="p-field-top-block">
                                <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                            </span>
                        </div>
                    </script>
                    <script data-js-template="template_add_block_show_remove_hover" type="text/x-tmpl">
                        <div class="p-block" data-js-block-group="addBlockShowRemoveHover">
                            <?php $form->attributeView( 'add_block_show_remove_hover', false, array( 'label' => null ) ); ?>
                            <span class="p-field-top-block">
                                <span class="btn btn-xs p-on-field-hover" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                            </span>
                        </div>
                    </script>
                    <script data-js-template="template_add_block_max_3_additional_fields" type="text/x-tmpl">
                        <div class="p-block" data-js-block-group="addBlockMax3AdditionalFields">
                            <?php $form->attributeView( 'add_block_max_3_additional_fields', false, array( 'label' => null ) ); ?>
                            <span class="p-field-top-block">
                                <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                            </span>
                        </div>
                    </script>
                    <script data-js-template="template_add_block_min_2_additional_fields" type="text/x-tmpl">
                        <div class="p-block" data-js-block-group="addBlockMin2AdditionalFields">
                            <?php $form->attributeView( 'add_block_min_2_additional_fields', false, array( 'label' => null ) ); ?>
                            <span class="p-field-top-block">
                                <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                            </span>
                        </div>
                    </script>
                    <script data-js-template="fieldsGroup1" type="text/x-tmpl">
                        <div class="p-block" data-js-block-group="addBlockfieldsGroup1">
                            <h4>Fields group</h4>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'add_block_fields_group_field_1', false ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'add_block_fields_group_field_2', false ); ?>
                                </div>
                            </div>
                            <div class="p-field-at-top-block">
                                <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                            </div>
                        </div>
                    </script>
                    <script data-js-template="fieldsGroup2" type="text/x-tmpl">
                        <div class="p-block" data-js-block-group="addBlockfieldsGroup2">
                            <h4>Fields group 2</h4>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'add_block_fields_group_field_3', false ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'add_block_fields_group_field_4', false ); ?>
                                </div>
                            </div>
                            <div class="p-field-at-top-block">
                                <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                            </div>
                        </div>
                    </script>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="p-block">
                                <?php $form->attributeView( 'add_block_default', 0 ); ?>
                                <?php foreach( array_slice( $form->cloneIndexList( 'add_block_default' ), 1 ) as $index ) { ?>
                                    <div class="p-block" data-js-block-group="addBlockDefault">
                                        <?php $form->attributeView( 'add_block_default', $index, array( 'label' => null ) ); ?>
                                        <span class="p-field-top-block">
                                            <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                                        </span>
                                    </div>
                                <?php }?>
                                <span class="p-field-bottom-block">
                                    <span class="p-action-link" data-js-add-block="template_add_block_default" data-js-add-block-group="addBlockDefault"><i class="fa fa-plus"></i> add field</span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="p-block">
                                <?php $form->attributeView( 'add_block_show_remove_hover', 0 ); ?>
                                <?php foreach( array_slice( $form->cloneIndexList( 'add_block_show_remove_hover' ), 1 ) as $index ) { ?>
                                    <div class="p-block" data-js-block-group="addBlockDefault">
                                        <?php $form->attributeView( 'add_block_show_remove_hover', $index, array( 'label' => null ) ); ?>
                                        <span class="p-field-top-block">
                                            <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                                        </span>
                                    </div>
                                <?php }?>
                                <span class="p-field-bottom-block">
                                    <span class="p-action-link" data-js-add-block="template_add_block_show_remove_hover" data-js-add-block-group="addBlockShowRemoveHover"><i class="fa fa-plus"></i> add field</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="p-block">
                                <?php $form->attributeView( 'add_block_max_3_additional_fields', 0 ); ?>
                                <?php foreach( array_slice( $form->cloneIndexList( 'add_block_max_3_additional_fields' ), 1 ) as $index ) { ?>
                                    <div class="p-block" data-js-block-group="addBlockMax3AdditionalFields">
                                        <?php $form->attributeView( 'add_block_max_3_additional_fields', $index, array( 'label' => null ) ); ?>
                                        <span class="p-field-top-block">
                                            <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                                        </span>
                                    </div>
                                <?php }?>
                                <span class="p-field-bottom-block">
                                    <span class="p-action-link" data-js-add-block="template_add_block_max_3_additional_fields" data-js-add-block-max="3" data-js-add-block-group="addBlockMax3AdditionalFields"><i class="fa fa-plus"></i> add field</span>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="p-block">
                                <?php $form->attributeView( 'add_block_min_2_additional_fields', 0 ); ?>
                                <?php foreach( array_slice( $form->cloneIndexList( 'add_block_min_2_additional_fields' ), 1 ) as $index ) { ?>
                                    <div class="p-block" data-js-block-group="addBlockMin2AdditionalFields">
                                        <?php $form->attributeView( 'add_block_min_2_additional_fields', $index, array( 'label' => null ) ); ?>
                                        <span class="p-field-top-block">
                                            <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                                        </span>
                                    </div>
                                <?php }?>
                                <span class="p-field-bottom-block">
                                    <span class="p-action-link" data-js-add-block="template_add_block_min_2_additional_fields" data-js-add-block-min="2" data-js-add-block-group="addBlockMin2AdditionalFields"><i class="fa fa-plus"></i> add field</span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr class="p-flat" />
                    <div>
                        <div class="p-block">
                            <h4>Fields group with 1 predefined</h4>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'add_block_fields_group_field_1', 0 ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'add_block_fields_group_field_2', 0 ); ?>
                                </div>
                            </div>
                        </div>
                        <?php foreach( array_slice( $form->cloneIndexList( 'add_block_fields_group_field_1' ), 1 ) as $index ) { ?>
                            <div class="p-block" data-js-block-group="addBlockfieldsGroup1">
                                <h4>Fields group</h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'add_block_fields_group_field_1', $index ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'add_block_fields_group_field_2', $index ); ?>
                                    </div>
                                </div>
                                <div class="p-field-at-top-block">
                                    <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                                </div>
                            </div>
                        <?php }?>
                        <span class="btn btn-sm" data-js-add-block="fieldsGroup1" data-js-insert-position="before" data-js-add-block-group="addBlockfieldsGroup1"><i class="fa fa-plus"></i> add fields group 1</span>
                    </div>
                    <hr class="p-flat" />
                    <div>
                        <?php foreach( $form->cloneIndexList( 'add_block_fields_group_field_3' ) as $index ) { ?>
                            <div class="p-block" data-js-block-group="addBlockfieldsGroup2">
                                <h4>Fields group</h4>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'add_block_fields_group_field_3', $index ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'add_block_fields_group_field_4', $index ); ?>
                                    </div>
                                </div>
                                <div class="p-field-at-top-block">
                                    <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                                </div>
                            </div>
                        <?php }?>
                        <span class="btn btn-sm" data-js-add-block="fieldsGroup2" data-js-insert-position="before" data-js-add-block-group="addBlockfieldsGroup2"><i class="fa fa-plus"></i> add fields group 2</span>
                    </div>
                    <hr class="p-flat" />
                    <div class="p-subtitle text-left">
                        <span class="p-title-side">Toggle block</span>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>Simple</label>
                            <br/>
                            <div class="btn-group">
                                <button type="button" class="btn" data-js-toggle-block="buttonShowHideBlock">Toggle</button>
                                <button type="button" class="btn" data-js-show-block="buttonShowHideBlock">Show</button>
                                <button type="button" class="btn" data-js-hide-block="buttonShowHideBlock">Hide</button>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <br>
                            <label>The
                                <span class="p-colored-text" data-js-related-block="buttonShowHideBlock" data-js-related-toggled-class="p-colored-text">block</span> is
                                <span data-js-related-block="buttonShowHideBlock" data-js-related-hidden-class="hide">visible</span>
                                <span class="hide" data-js-related-block="buttonShowHideBlock" data-js-related-shown-class="hide">hidden</span>
                            </label>
                            <br/>
                            <button type="button" class="btn" data-js-toggle-block="buttonShowHideBlock" data-js-related-toggled-class="p-active-btn">Toggle</button>
                            <button type="button" class="btn hide" data-js-show-block="buttonShowHideBlock" data-js-related-shown-class="hide">Show</button>
                            <button type="button" class="btn" data-js-hide-block="buttonShowHideBlock" data-js-related-hidden-class="hide">Hide</button>
                        </div>
                    </div>
                    <div data-js-block="buttonShowHideBlock" class="collapse">
                        <br/>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_simple_field_1' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_simple_field_2' ); ?>
                            </div>
                        </div>
                    </div>
                    <hr class="p-flat" />
                    <?php $form->attributeView( 'toggle_block_show_block' ); ?>
                    <div data-js-block="checkboxShowBlock" class="collapse">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_show_block_field_1' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_show_block_field_2' ); ?>
                            </div>
                        </div>
                    </div>
                    <hr class="p-flat" />
                    <?php $form->attributeView( 'toggle_block_hide_block' ); ?>
                    <div data-js-block="checkboxHideBlock" class="collapse">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_hide_block_field_1' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_hide_block_field_2' ); ?>
                            </div>
                        </div>
                    </div>
                    <hr class="p-flat" />
                    <?php $form->attributeView( 'toggle_block_show_hide_select' ); ?>
                    <div data-js-block="selectShowHideBlock" class="collapse">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_show_hide_select_field_1' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_show_hide_select_field_2' ); ?>
                            </div>
                        </div>
                    </div>
                    <hr class="p-flat" />
                    <div class="row">
                        <div class="col-sm-6">
                            <?php $form->attributeView( 'toggle_block_show_hide_radio' ); ?>
                        </div>
                        <div class="col-sm-6">
                            <div data-js-block="radioShowHideBlock" class="collapse">
                                <?php $form->attributeView( 'toggle_block_show_hide_radio_field_1' ); ?>
                                <?php $form->attributeView( 'toggle_block_show_hide_radio_field_2' ); ?>
                            </div>
                        </div>
                    </div>
                    <hr class="p-flat" />
                    <div class="btn-group">
                        <button type="button" class="btn" data-js-toggle-block="buttonShowBlock1" data-js-related-shown-class="p-active-btn">Block 1</button>
                        <button type="button" class="btn" data-js-toggle-block="buttonShowBlock2" data-js-related-shown-class="p-active-btn">Block 2</button>
                        <button type="button" class="btn" data-js-toggle-block="buttonShowBlock3" data-js-related-shown-class="p-active-btn">Block 3</button>
                    </div>
                    <br/>
                    <br/>
                    <div data-js-block="buttonShowBlock1" data-js-toggle-group="buttonShowGroup" class="collapse">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_buttons_field_1' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_buttons_field_2' ); ?>
                            </div>
                        </div>
                    </div>
                    <div data-js-block="buttonShowBlock2" data-js-toggle-group="buttonShowGroup" class="collapse">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_buttons_field_3' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_buttons_field_4' ); ?>
                            </div>
                        </div>
                    </div>
                    <div data-js-block="buttonShowBlock3" data-js-toggle-group="buttonShowGroup" class="collapse">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_buttons_field_5' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_buttons_field_6' ); ?>
                            </div>
                        </div>
                    </div>
                    <hr class="p-flat" />
                    <?php $form->attributeView( 'toggle_block_select_block' ); ?>
                    <div data-js-block="selectBlock1" data-js-toggle-group="selectBlockGroup" class="collapse">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_select_block_field_1' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_select_block_field_2' ); ?>
                            </div>
                        </div>
                    </div>
                    <div data-js-block="selectBlock2" data-js-toggle-group="selectBlockGroup" class="collapse">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_select_block_field_3' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_select_block_field_4' ); ?>
                            </div>
                        </div>
                    </div>
                    <div data-js-block="selectBlock3" data-js-toggle-group="selectBlockGroup" class="collapse">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_select_block_field_5' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'toggle_block_select_block_field_6' ); ?>
                            </div>
                        </div>
                    </div>
                    <hr class="p-flat" />
                    <div class="row">
                        <div class="col-sm-6">
                            <?php $form->attributeView( 'toggle_block_radio_blocks' ); ?>
                        </div>
                        <div class="col-sm-6">
                            <div data-js-block="radioBlock1" data-js-toggle-group="radioBlockGroup" class="collapse">
                                <?php $form->attributeView( 'toggle_block_radio_blocks_field_1' ); ?>
                                <?php $form->attributeView( 'toggle_block_radio_blocks_field_2' ); ?>
                            </div>
                            <div data-js-block="radioBlock2" data-js-toggle-group="radioBlockGroup" class="collapse">
                                <?php $form->attributeView( 'toggle_block_radio_blocks_field_3' ); ?>
                                <?php $form->attributeView( 'toggle_block_radio_blocks_field_4' ); ?>
                            </div>
                            <div data-js-block="radioBlock3" data-js-toggle-group="radioBlockGroup" class="collapse">
                                <?php $form->attributeView( 'toggle_block_radio_blocks_field_5' ); ?>
                                <?php $form->attributeView( 'toggle_block_radio_blocks_field_6' ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="p-subtitle text-left">
                        <span class="p-title-side">Toggle template block</span>
                    </div>
                    <div class="panel panel-fp">
                        <div class="panel-body">
                            On show blocks are created in specified place from predefined in &lt;script&gt; tag template, on remove - are removed from html. So while hidden they are not in html - any fields in it won't be submited.
                        </div>
                    </div>
                    <script data-js-template="buttonCreatedTemplate" type="text/x-tmpl">
                        <div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'toggle_template_block_button_field_1' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'toggle_template_block_button_field_2' ); ?>
                                </div>
                            </div>
                        </div>
                    </script>
                    <script data-js-template="simpleCreatedTemplate" type="text/x-tmpl">
                        <div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'toggle_template_block_simple_field_1' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'toggle_template_block_simple_field_2' ); ?>
                                </div>
                            </div>
                        </div>
                    </script>
                    <script data-js-template="radioCreatedTemplate" type="text/x-tmpl">
                        <div>
                            <?php $form->attributeView( 'toggle_template_block_radio_field_1' ); ?>
                            <?php $form->attributeView( 'toggle_template_block_radio_field_2' ); ?>
                        </div>
                    </script>
                    <div class="p-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Simple</label>
                                <br/>
                                <div class="btn-group">
                                    <button type="button" class="btn" data-js-toggle-block="buttonTemplateBlock:buttonCreatedTemplate">Toggle</button>
                                    <button type="button" class="btn" data-js-show-block="buttonTemplateBlock:buttonCreatedTemplate">Show</button>
                                    <button type="button" class="btn" data-js-hide-block="buttonTemplateBlock:buttonCreatedTemplate">Hide</button>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <br>
                                <label>The
                                    <span class="p-colored-text" data-js-related-block="buttonTemplateBlock" data-js-related-toggled-class="p-colored-text">block</span> is
                                    <span data-js-related-block="buttonTemplateBlock" data-js-related-hidden-class="hide">visible</span>
                                    <span class="hide" data-js-related-block="buttonTemplateBlock" data-js-related-shown-class="hide">hidden</span>
                                </label>
                                <br/>
                                <button type="button" class="btn" data-js-toggle-block="buttonTemplateBlock:buttonCreatedTemplate" data-js-related-toggled-class="p-active-btn">Toggle</button>
                                <button type="button" class="btn" data-js-show-block="buttonTemplateBlock:buttonCreatedTemplate" data-js-related-shown-class="hide">Show</button>
                                <button type="button" class="btn hide" data-js-hide-block="buttonTemplateBlock:buttonCreatedTemplate" data-js-related-hidden-class="hide">Hide</button>
                            </div>
                        </div>
                    </div>
                    <hr class="p-flat" />
                    <div class="p-block">
                        <?php $form->attributeView( 'toggle_template_block_show_block' ); ?>
                    </div>
                    <hr class="p-flat" />
                    <div class="p-block">
                        <?php $form->attributeView( 'toggle_template_block_hide_block' ); ?>
                    </div>
                    <hr class="p-flat" />
                    <div class="p-block">
                        <?php $form->attributeView( 'toggle_template_block_show_hide_select' ); ?>
                    </div>
                    <hr class="p-flat" />
                    <div class="row">
                        <div class="col-sm-6">
                            <?php $form->attributeView( 'toggle_template_block_show_hide_radio' ); ?>
                        </div>
                        <div class="col-sm-6" data-js-block="radioTemplateRoot">
                        </div>
                    </div>
                    <hr class="p-flat" />
                    <div class="p-block">
                        <script data-js-template="buttonCreatedTemplate1" type="text/x-tmpl">
                            <div data-js-toggle-group="buttonTemplateGroup">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_buttons_field_1' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_buttons_field_2' ); ?>
                                    </div>
                                </div>
                            </div>
                        </script>
                        <script data-js-template="buttonCreatedTemplate2" type="text/x-tmpl">
                            <div data-js-toggle-group="buttonTemplateGroup">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_buttons_field_3' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_buttons_field_4' ); ?>
                                    </div>
                                </div>
                            </div>
                        </script>
                        <script data-js-template="buttonCreatedTemplate3" type="text/x-tmpl">
                            <div data-js-toggle-group="buttonTemplateGroup">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_buttons_field_5' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_buttons_field_6' ); ?>
                                    </div>
                                </div>
                            </div>
                        </script>
                        <div class="btn-group">
                            <button type="button" class="btn" data-js-toggle-block="buttonTemplateBlock1:buttonCreatedTemplate1" data-js-related-shown-class="p-active-btn">Block 1</button>
                            <button type="button" class="btn" data-js-toggle-block="buttonTemplateBlock2:buttonCreatedTemplate2" data-js-related-shown-class="p-active-btn">Block 2</button>
                            <button type="button" class="btn" data-js-toggle-block="buttonTemplateBlock3:buttonCreatedTemplate3" data-js-related-shown-class="p-active-btn">Block 3</button>
                        </div>
                        <br/>
                        <br/>
                    </div>
                    <hr class="p-flat" />
                    <div class="p-block">
                        <script data-js-template="selectCreatedTemplate1" type="text/x-tmpl">
                            <div data-js-toggle-group="selectTemplateGroup">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_select_block_field_1' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_select_block_field_2' ); ?>
                                    </div>
                                </div>
                            </div>
                        </script>
                        <script data-js-template="selectCreatedTemplate2" type="text/x-tmpl">
                            <div data-js-toggle-group="selectTemplateGroup">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_select_block_field_3' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_select_block_field_4' ); ?>
                                    </div>
                                </div>
                            </div>
                        </script>
                        <script data-js-template="selectCreatedTemplate3" type="text/x-tmpl">
                            <div data-js-toggle-group="selectTemplateGroup">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_select_block_field_5' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'toggle_template_block_select_block_field_6' ); ?>
                                    </div>
                                </div>
                            </div>
                        </script>
                        <?php $form->attributeView( 'toggle_template_block_select_block' ); ?>
                    </div>
                    <hr class="p-flat" />
                    <script data-js-template="radioCreatedTemplate1" type="text/x-tmpl">
                        <div data-js-toggle-group="radioTemplateGroup">
                            <?php $form->attributeView( 'toggle_template_block_radio_blocks_field_1' ); ?>
                            <?php $form->attributeView( 'toggle_template_block_radio_blocks_field_2' ); ?>
                        </div>
                    </script>
                    <script data-js-template="radioCreatedTemplate2" type="text/x-tmpl">
                        <div data-js-toggle-group="radioTemplateGroup">
                            <?php $form->attributeView( 'toggle_template_block_radio_blocks_field_3' ); ?>
                            <?php $form->attributeView( 'toggle_template_block_radio_blocks_field_4' ); ?>
                        </div>
                    </script>
                    <script data-js-template="radioCreatedTemplate3" type="text/x-tmpl">
                        <div data-js-toggle-group="radioTemplateGroup">
                            <?php $form->attributeView( 'toggle_template_block_radio_blocks_field_5' ); ?>
                            <?php $form->attributeView( 'toggle_template_block_radio_blocks_field_6' ); ?>
                        </div>
                    </script>
                    <div class="row">
                        <div class="col-sm-6">
                            <?php $form->attributeView( 'toggle_template_block_radio_blocks' ); ?>
                        </div>
                        <div class="col-sm-6" data-js-block="radioGroupTemplateRoot">
                        </div>
                    </div>
                    <div class="p-subtitle text-left">
                        <span class="p-title-side">Nested template block</span>
                    </div>
                    <script data-js-template="nestedLVL1Template" type="text/x-tmpl">
                        <div class="p-block">
                            <?php $form->attributeView( 'nested_template_block_checkbox_lvl_1' ); ?>
                        </div>
                    </script>
                    <script data-js-template="nestedLVL2Template" type="text/x-tmpl">
                        <div>
                            <?php $form->attributeView( 'nested_template_block_checkbox_lvl_2' ); ?>
                        </div>
                    </script>
                    <div class="row">
                        <div class="col-sm-12 p-block">
                            <?php $form->attributeView( 'nested_template_block_checkbox' ); ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>