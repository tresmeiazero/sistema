<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/js-elements.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">js elements&nbsp;&nbsp;<i class="fa fa-list"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Input group</span>
                        </div>
                        <div class="row">
                            <?php
                                $delimiterCounter = 0;
                                $subtitleList = array(
                                    'input_group_code'                => array( 12 ),
                                    'icon_default'                    => array( 6, 'icon' ),
                                    'password'                        => array( 6, 'Password' ),
                                    'spinner_default'                 => array( 6, 'Spinner' ),
                                    'imageupload_single'              => array( 4, 'Image file upload' ),
                                    'imageupload_multiple'            => array( 12 ),
                                    'checkbox_toggle_all_other_input' => array( 6, 'Checkboxes' ),
                                    'radiobutton_sub_checkbox'        => array( 12, true ),
                                    'checkbox_sub_radiobutton_cherry' => array( 4 ),
                                    'checkbox_sub_checkbox'           => array( 12, true ),
                                    'checkbox_sub_checkbox_cherry'    => array( 4 )
                                );
                                $columnSize = 6;
                            ?>
                            <?php foreach( $form->attributeList() as $attributeIdentifier ) { ?>
                                <?php if( isset( $subtitleList[ $attributeIdentifier ] ) ) { ?>
                                    </div>
                                    <?php if( isset( $subtitleList[ $attributeIdentifier ][ 1 ] ) && $subtitleList[ $attributeIdentifier ][ 1 ] === true ) { ?>
                                        <hr class="p-flat">
                                    <?php } elseif( !empty( $subtitleList[ $attributeIdentifier ][ 1 ] ) ) { ?>
                                        <div class="p-subtitle text-left">
                                            <span class="p-title-side"><?php echo $subtitleList[ $attributeIdentifier ][ 1 ]; ?></span>
                                        </div>
                                    <?php } ?>
                                    <div class="row">
                                    <?php 
                                        $columnSize = $subtitleList[ $attributeIdentifier ][ 0 ]; 
                                        $delimiterCounter = 0;
                                    ?>
                                <?php } elseif( $delimiterCounter % ( 12 / $columnSize ) == 0 && $delimiterCounter != 0 ) { ?>
                                    </div><div class="row">
                                <?php } ?>
                                <div class="col-sm-<?php echo $columnSize; ?>">
                                    <?php $form->attributeView( $attributeIdentifier ); ?>
                                </div>
                                <?php $delimiterCounter++; ?>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="text-right">
                            <button class="btn" type="submit">submit</button>
                            <button class="btn" type="reset">reset</button>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>