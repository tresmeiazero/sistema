<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-order-service.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">Order service&nbsp;&nbsp;<i class="fa fa-pencil-square-o"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Your details</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'name' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'company' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'email' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'contact_phone' ); ?>
                            </div>
                        </div>
                        <div class="p-subtitle text-left">
                            <span class="p-title-side">Order details</span>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'service' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'budget' ); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'date_from' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'date_to' ); ?>
                            </div>
                        </div>
                        <?php $form->attributeView( 'message' ); ?>
                        <div class="text-right">
                            <button class="btn" type="submit" name="confirm"><i class="fa fa-check-square-o"></i>&nbsp;order service</button>
                            <a class="btn" href="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>"><i class="fa fa-ban"></i>&nbsp;cancel</a>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>