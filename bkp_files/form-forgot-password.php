<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-forgot-password.yml' );
    $formNewPassword = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-new-password.yml' );
    // set a new password for a user who got this recovery link via an email
    if( !empty( $_GET[ 'action' ] ) && $_GET[ 'action' ] === 'user_newpassword' && !empty( $_GET[ 'email' ] ) && !empty( $_GET[ 'key' ] ) ) {
        $parameterList = $form->configuration[ 'mail' ][ 'message' ][ 'forgotpassword' ][ 'template_parameter_list' ];
        $filter = array( 'prop_form_config_file' => $form->configFile );
        $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
        $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = $_GET[ 'key' ];
        $form->initStorage();
        $forgotPasswordCollectionSet = $form->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
        if( $forgotPasswordCollectionSet[ 'count' ] === 1 ) {
            $forgotPasswordCollection = $forgotPasswordCollectionSet[ 'list' ][ '0' ];
            $actionUserNewPassword = array( 'form' => true );
            if( $formNewPassword->isValid ) {
                $newPasswordHash = $formNewPassword->data[ 'attribute_list' ][ 'new_password' ][ 'final_value' ];
                $formRegistration = FormsPlusFramework::initDummy( 'forms-plus-framework/app/config/forms/form-registration.yml' );
                $parameterList = $formRegistration->configuration[ 'mail' ][ 'message' ][ 'registration' ][ 'template_parameter_list' ];
                $filter = array( 'prop_form_config_file' => $formRegistration->configFile );
                $filter[ 'attr_' . $parameterList[ 'email_attribute_id' ] ] = $_GET[ 'email' ];
                $filter[ 'attr_' . $parameterList[ 'hash_key_attribute_id' ] ] = '';
                $formRegistration->initStorage();
                $userCollectionSet = $formRegistration->storage->fetchCollectionList( null, 1, 0, null, null, $filter );
                if( $userCollectionSet[ 'count' ] === 1 ) {
                    $userCollection = $userCollectionSet[ 'list' ][ '0' ];
                    $userCollection[ 'attribute_list' ][ $parameterList[ 'password_attribute_id' ] ][ 'final_value' ] = $newPasswordHash;
                    $formRegistration->storage->updateCollection( $userCollection );
                    $form->storage->removeCollection( $forgotPasswordCollection[ 'collection_id' ] );
                    $actionUserNewPassword[ 'form' ] = array( 'valid', 'Your password has been successfully changed.' );
                } elseif( $userCollectionSet[ 'count' ] === 0 ) {
                    $actionUserNewPassword[ 'form' ] = array( 'error', 'The password recovery is impossible.', 'A user with such email address does not exist.' );
                } elseif( $userCollectionSet[ 'count' ] > 1 ) {
                    $actionUserNewPassword[ 'form' ] = array( 'error', 'The password recovery is impossible.', 'A user with such email address does not exist.' );
                }
            }
        } elseif( $forgotPasswordCollectionSet[ 'count' ] === 0 ) {
            $actionUserNewPassword = array( 'error', 'The password recovery is impossible.', 'The submitted key is invalid.' );
        } elseif( $forgotPasswordCollectionSet[ 'count' ] > 1 ) {
            $actionUserNewPassword = array( 'error', 'The password recovery has failed.', 'An internal error occurred. Please, contact the website administrator.' );
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo isset( $actionUserNewPassword ) ? $_SERVER[ 'REQUEST_URI' ] : strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <?php if( isset( $actionUserNewPassword ) ) { ?>
                        <div class="p-title text-left">
                            <span class="p-title-side">New password</span>
                        </div>
                        <?php if( !isset( $actionUserNewPassword[ 'form' ] ) ) { ?>
                            <div class="alert alert-<?php echo $actionUserNewPassword[ 0 ]; ?>">
                                <strong><i class="fa fa-<?php echo $actionUserNewPassword[ 0 ] === 'error' ? 'times' : 'check'; ?>"></i> <?php echo $actionUserNewPassword[ 1 ]; ?></strong>
                                <?php echo isset( $actionUserNewPassword[ 2 ] ) ? $actionUserNewPassword[ 2 ] : ''; ?>
                            </div>
                            <div class="text-right">
                                <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                            </div>
                        <?php } else { ?>
                            <?php if( is_array( $actionUserNewPassword[ 'form' ] ) ) { ?>
                                <div class="alert alert-<?php echo $actionUserNewPassword[ 'form' ][ 0 ]; ?>">
                                    <strong><i class="fa fa-<?php echo $actionUserNewPassword[ 'form' ][ 0 ] === 'error' ? 'times' : 'check'; ?>"></i> <?php echo $actionUserNewPassword[ 'form' ][ 1 ]; ?></strong>
                                    <?php echo isset( $actionUserNewPassword[ 'form' ][ 2 ] ) ? $actionUserNewPassword[ 'form' ][ 2 ] : ''; ?>
                                </div>
                                <div class="text-right">
                                    <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                                </div>
                            <?php } else { ?>
                                <?php if( !$formNewPassword->isValid && $formNewPassword->isSubmitted ) { ?>
                                    <?php foreach( $formNewPassword->errorList as $attributeID => $itemList ) { ?>
                                        <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $formNewPassword->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                            <?php if( count( $itemList ) == 1 ) { ?>
                                                <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                            <?php } else { ?>
                                                <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <?php if( !$formNewPassword->isValid ) { ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php $formNewPassword->attributeView( 'new_password' ); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php $formNewPassword->attributeView( 'confirm_new_password' ); ?>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <button class="btn" type="submit" name="confirm"><i class="fa fa-lock"></i>&nbsp;Update password</button>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="p-title text-left">
                            <span class="p-title-side">Forgot password?</span>
                        </div>
                        <?php if( $form->isValid ) { ?>
                            <div class="alert alert-valid"><strong><i class="fa fa-check"></i> An email has been sent.</strong> This email contains a link you need to click so that we can confirm that the correct user is getting the new password.</div>
                            <div class="text-right">
                                <a href="<?php echo strtok( $_SERVER[ 'REQUEST_URI' ], '?' ); ?>" class="btn">reload</a>
                            </div>
                        <?php } elseif( $form->isSubmitted ) { ?>
                            <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                                <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                    <?php if( count( $itemList ) == 1 ) { ?>
                                        <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                    <?php } else { ?>
                                        <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <?php if( !$form->isValid ) { ?>
                            <p>Enter your email and we'll send you your recovery details.</p>
                            <div class="row">
                                <div class="col-sm-6">
                                    <?php $form->attributeView( 'email' ); ?>
                                </div>
                                <div class="col-sm-6">
                                    <button class="btn" type="submit" name="confirm"><i class="fa fa-envelope-o"></i>&nbsp;send email</button>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>