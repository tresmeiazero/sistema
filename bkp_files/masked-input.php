<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/demos/masked-input.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">Masked input&nbsp;&nbsp;<i class="fa fa-list"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <div class="panel panel-fp">
                            <div class="panel-heading">
                                <h3 class="panel-title">Masked input notes</h3>
                            </div>
                            <div class="panel-body">
                                <ul>
                                    <li>
                                        The following mask definitions are predefined:
                                        <ul class="list-unstyled">
                                            <li>a - Represents an alpha character (A-Z, a-z)</li>
                                            <li>9 - Represents a numeric character (0-9)</li>
                                            <li>* - Represents an alphanumeric character (A-Z, a-z, 0-9)</li>
                                            <li>~ - Represents a plus/minus character (+, -)</li>
                                            <li>h - Represents a hex color character (A-F, a-f, 0-9)</li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <?php $delimiterCounter = 0; ?>
                            <?php foreach( $form->attributeList() as $attributeIdentifier ) { ?>
                                <?php if( $delimiterCounter % 2 == 0 && $delimiterCounter != 0 ) { ?>
                                    </div><div class="row">
                                <?php } ?>
                                <div class="col-sm-6">
                                    <?php $form->attributeView( $attributeIdentifier ); ?>
                                </div>
                                <?php $delimiterCounter++; ?>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="text-right">
                            <button class="btn" type="submit">submit</button>
                            <button class="btn" type="reset">reset</button>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>