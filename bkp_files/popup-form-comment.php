<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/popup-form-comment.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="<?php $form->designCSSClasses(); ?>">
                <div class="p-btn-pannel p-fixed p-pos-left">
                    <label for="popup-form-id" class="btn"><i class="fa fa-hand-pointer-o"></i></label>
                </div>
                <div>
                    <input type="checkbox" id="popup-form-id" class="hide p-show-check" />
                    <!-- Remove label below to remove background -->
                    <label for="popup-form-id" class="p-popup-bg"></label>
                    <div class="p-popup p-action-block">
                        <form class="p-form p-shadowed p-form-sm" method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                            <div class="p-title text-left">
                                <span class="p-title-side">Comment&nbsp;&nbsp;<i class="fa fa-comment-o"></i></span>
                            </div>
                            <?php if( $form->isValid ) { ?>
                                <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                            <?php } elseif( $form->isSubmitted ) { ?>
                                <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                                    <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                        <?php if( count( $itemList ) == 1 ) { ?>
                                            <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                        <?php } else { ?>
                                            <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <?php if( !$form->isValid ) { ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'name' ); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php $form->attributeView( 'email' ); ?>
                                    </div>
                                </div>
                                <?php $form->attributeView( 'comment' ); ?>
                                <?php $form->attributeView( 'captcha' ); ?>
                                <div class="clearfix"></div>
                                <?php $form->attributeView( 'notify' ); ?>
                                <div class="text-right">
                                    <button class="btn" type="submit" name="confirm"><i class="fa fa-share-square-o"></i>&nbsp;post comment</button>
                                </div>
                            <?php } ?>
                            <label class="p-hide-form p-action-link" for="popup-form-id"><i class="fa fa-times"></i></label>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>