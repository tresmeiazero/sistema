<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\Helper\AdminPanel;
    new AdminPanel( 'forms-plus-framework/app/config/admin.yml' );
?>