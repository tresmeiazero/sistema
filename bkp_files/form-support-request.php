<?php
    require_once __DIR__ . '/forms-plus-framework/vendor/autoload.php';
    use FormsPlus\Framework\FormsPlusFramework as FormsPlusFramework;
    $form = new FormsPlusFramework( 'forms-plus-framework/app/config/forms/form-support-request.yml' );
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Forms Plus: PHP</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php FormsPlusFramework::loadStyles(); ?>
        <link rel="stylesheet" href="<?php echo FormsPlusFramework::getStaticVar( 'asset_directory_uri' ); ?>css/base.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER[ 'REQUEST_URI' ]; ?>" class="<?php $form->designCSSClasses(); ?>" data-js-validate="true" data-js-highlight-state-msg="true" data-js-show-valid-msg="true">
                <div class="p-form p-shadowed p-form-sm">
                    <div class="p-title text-left">
                        <span class="p-title-side">Support request&nbsp;&nbsp;<i class="fa fa-life-ring"></i></span>
                    </div>
                    <?php if( $form->isValid ) { ?>
                        <div class="alert alert-valid"><strong><i class="fa fa-check"></i> Thank you:</strong> message example.</div>
                    <?php } elseif( $form->isSubmitted ) { ?>
                        <?php foreach( $form->errorList as $attributeID => $itemList ) { ?>
                            <div class="alert alert-error"><strong><i class="fa fa-times"></i> <?php echo $form->configuration[ 'attribute_list' ][ $attributeID ][ 'name' ]; ?>:</strong>
                                <?php if( count( $itemList ) == 1 ) { ?>
                                    <?php foreach( $itemList as $errorList ) echo implode( ' ', $errorList ); ?>
                                <?php } else { ?>
                                    <ul><?php foreach( $itemList as $errorList ) echo '<li>' . implode( ' ', $errorList ) . '</li>'; ?></ul>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <?php if( !$form->isValid ) { ?>
                        <script data-js-template="templateAttachment" type="text/x-tmpl">
                            <div class="p-block" data-js-block-group="attachment">
                                <?php $form->attributeView( 'attachment', false, array( 'label' => null ) ); ?>
                                <span class="p-field-top-block">
                                    <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                                </span>
                            </div>
                        </script>
                        <div class="row">
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'name' ); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php $form->attributeView( 'email' ); ?>
                            </div>
                        </div>
                        <?php $form->attributeView( 'department' ); ?>
                        <?php $form->attributeView( 'message' ); ?>
                        <div class="p-block">
                            <?php $form->attributeView( 'attachment', 0 ); ?>
                            <?php foreach( array_slice( $form->cloneIndexList( 'attachment' ), 1 ) as $index ) { ?>
                                <div class="p-block" data-js-block-group="attachment">
                                    <?php $form->attributeView( 'attachment', $index, array( 'label' => null ) ); ?>
                                    <span class="p-field-top-block">
                                        <span class="btn btn-xs" data-js-remove-block="">remove <i class="fa fa-times"></i></span>
                                    </span>
                                </div>
                            <?php }?>
                            <span class="p-field-bottom-block">
                                <span class="p-action-link" data-js-add-block="templateAttachment" data-js-add-block-group="attachment"><i class="fa fa-plus"></i> add more</span>
                            </span>
                        </div>
                        <?php $form->attributeView( 'captcha' ); ?>
                        <div class="text-right">
                            <button class="btn" type="submit" name="confirm"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;send message</button>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>
        <?php FormsPlusFramework::loadScripts(); ?>
        <?php FormsPlusFramework::outputDebug(); ?>
    </body>
</html>